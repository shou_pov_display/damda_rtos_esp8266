# damda_rtos



## Getting started

담다 연동을 위한 소스 정리  

* damdaSDKLib
* docs
* external_Libs
* samples


### 아래 라이브러는 현재 이 폴더에 위차하고 Arduino library에 소프트 링크를 통해서 관리한다.
* damdaSDKLib
* external_Libs


### 실제 링크 현황

* ArduinoJson -> /home/jongju/damda_rtos_esp8266/external_Libs/ArduinoJson/
* arduino-mqtt-master -> /home/jongju/damda_rtos_esp8266/external_Libs/arduino-mqtt-master/
* damdaSDKLib -> /home/jongju/damda_rtos_esp8266/damdaSDKLib/
* jansson -> /home/jongju/damda_rtos_esp8266/external_Libs/jansson/


### 링크 방법

* ln -s /home/jongju/damda_rtos_esp8266/external_Libs/ArduinoJson/ ArduinoJson
* ln -s /home/jongju/damda_rtos_esp8266/external_Libs/arduino-mqtt-master/ arduino-mqtt-master
* ln -s /home/jongju/damda_rtos_esp8266/external_Libs/jansson/ jansson
* ln -s /home/jongju/damda_rtos_esp8266/damdaSDKLib/ damdaSDKLib