#ifndef __DAMDACORESDK_H__
#define __DAMDACORESDK_H__

#define 	_DEBUG_
#define 	_ARDUINO_
#define		_ESP8266_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arduino.h>
#include <jansson.h>
#include <MQTT.h>
#include <WiFiClientSecure.h>
#include <time.h>

#define DAMDA_SDK_VERSION "Damda_Arduino_Sdk_v1.1"

//#define BUF_LEN 1024
#define QOS 1
#define MESSAGE_TYPE_RESPONSE 1
#define MESSAGE_TYPE_READ 2
#define MESSAGE_TYPE_WRITE 3
#define MESSAGE_TYPE_EXECUTE 4
#define MESSAGE_TYPE_DELETE_SYNC 5
#define MESSAGE_TYPE_DELETE_ASYNC 6
#define MESSAGE_TYPE_FIRMWARE_DOWNLOAD 7
#define MESSAGE_TYPE_READ_ASYNC 8
#define MESSAGE_TYPE_WRITE_ASYNC 9
#define MESSAGE_TYPE_EXECUTE_ASYNC 10

#define TOPIC_VERSION "1.0"
#define PROFILE_VERSION "1.0"
#define SERVICE_PROTOCOL_VERSION "1.0"

#define MAX_RESOURCE_SIZE 30

#define SID_BUFFER_LEN 50
#define ERROR_CODE_BUFFER_LEN 10
#define RETURN_CODE_BUFFER_LEN 10
#define TIME_BUFFER_LEN 15

// If device needs to connect to an Operation platform(AWS/DAMDA), enable the definition belows.
//#define Operation_Mode_Certification

typedef struct connectionInfo{
	const char* serverAddr;
	const char* serverPort;
	const char* apSsid;
	const char* apPassword;
	const char* authKey;
}DAMDA_connectionInfo;

typedef struct deviceStatusInfo{
	char* manufacturer;
	char* model;
	char* serial;
	char* firmwareVersion;
} DeviceStatusInfo;

typedef struct responseMessageInfo{
	#ifdef _ARDUINO_
	    char sid[SID_BUFFER_LEN];
	    int seq;
	    char* resourceUri[MAX_RESOURCE_SIZE];
	    char* stringValue[MAX_RESOURCE_SIZE];
	    char* time[MAX_RESOURCE_SIZE];
	    char* firmwareDownloadUrl;
	    char* firmwareVersion;
	#elif _LINUX_
		char sid[SID_BUFFER_LEN];
		char* resourceUri[MAX_RESOURCE_SIZE];
		char* stringValue[MAX_RESOURCE_SIZE];
		char* time[MAX_RESOURCE_SIZE];
		char* firmwareDownloadUrl;
		char* firmwareVersion;
	#endif
	int returnCode;
	int errorCode;

	int responseType;
	int responseArraySize;
} ResponseMessageInfo;

typedef struct notifyParamter{
	char* resourceUri;
	char* stringValue;
	char* time;
} NotifyParameter;

typedef void (*MSG_ARRIVED_CALLBACK_FUNCTION)(ResponseMessageInfo*);

extern int server_fd, client_fd;

extern int len, msg_size;

extern int connectionFailCallbackStatus;


extern int receiveServerInfoStatus;

extern const char* serverAddr;
extern const char* serverPort;
extern const char* apSsid;
extern const char* apPassword;
extern const char* authKey;

#ifdef Operation_Mode_Certification
// pem code for Operation platform(AWS/DAMDA) 
static const char ca_pem[] PROGMEM = R"EOF(
-----BEGIN CERTIFICATE-----
MIIEkjCCA3qgAwIBAgITBn+USionzfP6wq4rAfkI7rnExjANBgkqhkiG9w0BAQsF
ADCBmDELMAkGA1UEBhMCVVMxEDAOBgNVBAgTB0FyaXpvbmExEzARBgNVBAcTClNj
b3R0c2RhbGUxJTAjBgNVBAoTHFN0YXJmaWVsZCBUZWNobm9sb2dpZXMsIEluYy4x
OzA5BgNVBAMTMlN0YXJmaWVsZCBTZXJ2aWNlcyBSb290IENlcnRpZmljYXRlIEF1
dGhvcml0eSAtIEcyMB4XDTE1MDUyNTEyMDAwMFoXDTM3MTIzMTAxMDAwMFowOTEL
MAkGA1UEBhMCVVMxDzANBgNVBAoTBkFtYXpvbjEZMBcGA1UEAxMQQW1hem9uIFJv
b3QgQ0EgMTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALJ4gHHKeNXj
ca9HgFB0fW7Y14h29Jlo91ghYPl0hAEvrAIthtOgQ3pOsqTQNroBvo3bSMgHFzZM
9O6II8c+6zf1tRn4SWiw3te5djgdYZ6k/oI2peVKVuRF4fn9tBb6dNqcmzU5L/qw
IFAGbHrQgLKm+a/sRxmPUDgH3KKHOVj4utWp+UhnMJbulHheb4mjUcAwhmahRWa6
VOujw5H5SNz/0egwLX0tdHA114gk957EWW67c4cX8jJGKLhD+rcdqsq08p8kDi1L
93FcXmn/6pUCyziKrlA4b9v7LWIbxcceVOF34GfID5yHI9Y/QCB/IIDEgEw+OyQm
jgSubJrIqg0CAwEAAaOCATEwggEtMA8GA1UdEwEB/wQFMAMBAf8wDgYDVR0PAQH/
BAQDAgGGMB0GA1UdDgQWBBSEGMyFNOy8DJSULghZnMeyEE4KCDAfBgNVHSMEGDAW
gBScXwDfqgHXMCs4iKK4bUqc8hGRgzB4BggrBgEFBQcBAQRsMGowLgYIKwYBBQUH
MAGGImh0dHA6Ly9vY3NwLnJvb3RnMi5hbWF6b250cnVzdC5jb20wOAYIKwYBBQUH
MAKGLGh0dHA6Ly9jcnQucm9vdGcyLmFtYXpvbnRydXN0LmNvbS9yb290ZzIuY2Vy
MD0GA1UdHwQ2MDQwMqAwoC6GLGh0dHA6Ly9jcmwucm9vdGcyLmFtYXpvbnRydXN0
LmNvbS9yb290ZzIuY3JsMBEGA1UdIAQKMAgwBgYEVR0gADANBgkqhkiG9w0BAQsF
AAOCAQEAYjdCXLwQtT6LLOkMm2xF4gcAevnFWAu5CIw+7bMlPLVvUOTNNWqnkzSW
MiGpSESrnO09tKpzbeR/FoCJbM8oAxiDR3mjEH4wW6w7sGDgd9QIpuEdfF7Au/ma
eyKdpwAJfqxGF4PcnCZXmTA5YpaP7dreqsXMGz7KQ2hsVxa81Q4gLv7/wmpdLqBK
bRRYh5TmOTFffHPLkIhqhBGWJ6bt2YFGpn6jcgAKUj6DiAdjd4lpFw85hdKrCEVN
0FE6/V1dN2RMfjCyVSRCnTawXZwXgWHxyvkQAiSr6w10kY17RSlQOYiypok1JR4U
akcjMS9cmvqtmg5iUaQqqcT5NJ0hGA==
-----END CERTIFICATE-----
)EOF";
#else
// pem code for developmemt platform(KEA/DAMDA)
static const char ca_pem[] PROGMEM = R"EOF(
-----BEGIN CERTIFICATE-----
MIIDSjCCAjKgAwIBAgIQRK+wgNajJ7qJMDmGLvhAazANBgkqhkiG9w0BAQUFADA/
MSQwIgYDVQQKExtEaWdpdGFsIFNpZ25hdHVyZSBUcnVzdCBDby4xFzAVBgNVBAMT
DkRTVCBSb290IENBIFgzMB4XDTAwMDkzMDIxMTIxOVoXDTIxMDkzMDE0MDExNVow
PzEkMCIGA1UEChMbRGlnaXRhbCBTaWduYXR1cmUgVHJ1c3QgQ28uMRcwFQYDVQQD
Ew5EU1QgUm9vdCBDQSBYMzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEB
AN+v6ZdQCINXtMxiZfaQguzH0yxrMMpb7NnDfcdAwRgUi+DoM3ZJKuM/IUmTrE4O
rz5Iy2Xu/NMhD2XSKtkyj4zl93ewEnu1lcCJo6m67XMuegwGMoOifooUMM0RoOEq
OLl5CjH9UL2AZd+3UWODyOKIYepLYYHsUmu5ouJLGiifSKOeDNoJjj4XLh7dIN9b
xiqKqy69cK3FCxolkHRyxXtqqzTWMIn/5WgTe1QLyNau7Fqckh49ZLOMxt+/yUFw
7BZy1SbsOFU5Q9D8/RhcQPGX69Wam40dutolucbY38EVAjqr2m7xPi71XAicPNaD
aeQQmxkqtilX4+U9m5/wAl0CAwEAAaNCMEAwDwYDVR0TAQH/BAUwAwEB/zAOBgNV
HQ8BAf8EBAMCAQYwHQYDVR0OBBYEFMSnsaR7LHH62+FLkHX/xBVghYkQMA0GCSqG
SIb3DQEBBQUAA4IBAQCjGiybFwBcqR7uKGY3Or+Dxz9LwwmglSBd49lZRNI+DT69
ikugdB/OEIKcdBodfpga3csTS7MgROSR6cz8faXbauX+5v3gTt23ADq1cEmv8uXr
AvHRAosZy5Q6XkjEGB5YGV8eAlrwDPGxrancWYaLbumR9YbK+rlmM6pZW87ipxZz
R8srzJmwN0jP41ZL9c8PDHIyh8bwRLtTcm1D9SZImlJnt1ir/md2cXjbDaJWFBM5
JDGFoqgCWjBH4d1QB7wCCZAA62RjYJsWvIjJEubSfZGL+T0yjWW06XyxV3bqxbYo
Ob8VZRzI9neWagqNdwvYkQsEjgfbKbYK7p2CNTUQ  
-----END CERTIFICATE-----
)EOF";
#endif


#ifdef __cplusplus
extern "C" {
#endif

char* damda_getSDKVersion();

DAMDA_connectionInfo getConnectionInfo();

DeviceStatusInfo getDeviceInfo();

void damda_setupDeviceInfo(char* manufacturer,char* model,char* serial,char* firmwareVersion,char* deviceType);

int damda_isReceiveServerInfo();

void damda_setMQTTMsgReceiveCallback(MSG_ARRIVED_CALLBACK_FUNCTION func);

void damda_MQTT_Connect(char* mqttUser,char* mqttPassword,char* mqttAddress,int mqttPort,char* device_type);

void damda_DisConnectMQTT();

void damda_destroyMQTT();

bool damda_IsMQTTConnected();

int damda_Req_RegisterDevice();

int damda_Req_DeregisterDevice();

int damda_Req_DeleteDevice();

int damda_Req_UpdateDeviceInfo();

int damda_Req_SeverTimeInfo();

int damda_Notify_DeviceStatus(const char* notificationType,NotifyParameter notifyParams[],int arrayCount);

int damda_Rsp_RemoteControl(int controlType,const char* sid,const char* returnCode,char* resourceUri[],char* stringValue[],int arrayCount);

int damda_Rsp_RemoteControl_noEntity(int controlType,const char* sid,const char* returnCode);

int damda_Rsp_RemoteDelDevice(const char* sid, const char* returnCode);

int damda_Req_FirmwareDownload(char* downloadPath,char* fileName,char* firmwareDownloadUrl);

void damda_SetConnectInfo(char* server_addr,char* server_port,char* ap_ssid, char* ap_password,char* auth_key);

void damda_messageReceived_callback_loop();

char* getDid(char* message);

int getNArraySize(char* message);

char* getServiceId(char* topic);

int getMessageType(char* topic,char* message);

int getSid(char* message,char* mSid);

int getSeq(char* message,int* mSeq);

int getOperation(char* message,char* operation);

int getReturnCode(char* message,int* mReturnCode);

int getErrorCode(char* message,int* mErrorCode);

int getResourceUri(char* message,size_t index,char* mResourceUri);

int getNArraySize(char* message);

int getStringValue(char* message,size_t index,char* mStringValue);

int getTime(char* message,size_t index,char* mTime);

int getStringValueByResourceUri(char* message,char* n,char* mStringValue);

void getMsgType(char* topic,char* mMsgType);

void getOriginId(char* topic,char* mOriginId);

void getTargetId(char* topic,char* mTargetId);

void getTopicOprId(char* topic,char* mTopicOprId);

int isFirmwareUpdateMessage(char* message);

char *httpParser(char *s, const char *olds, const char *news);

void httpResponseController(char* header,char* body);

void damda_httpServerInit(int port);

void damda_httpServerThreadStart();

void damda_Secure();

char* damda_getSDKVersion();

#ifdef __cplusplus
}
#endif

#endif
