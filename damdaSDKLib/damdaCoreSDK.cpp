#include "damdaCoreSDK.h"

char deviceId[50];
char subscribeTopic[100];
char damda_deviceType[20];

int sequence = 0 ;
const int MAX_SEQUENCE = 60000;
MSG_ARRIVED_CALLBACK_FUNCTION callbackFunc = NULL;

WiFiClientSecure net;
MQTTClient client(2048);
	
int messageArrivedCallbackStatus;
DeviceStatusInfo deviceStatus;
DAMDA_connectionInfo damda_connInfo;

#ifdef _ESP8266_
	BearSSL::X509List cert(ca_pem);
	BearSSL::Session session;
#endif


void setClock() 
{
	//configTime(9 * 3600, 0, "pool.ntp.org", "pool.ntp.org");
	configTime(9 * 3600, 0, "kr.pool.ntp.org", "kr.pool.ntp.org");
	#ifdef _ARDUINO_
	Serial.print("at setClock -> Waiting for NTP time sync: ");
	#endif
	time_t now = time(nullptr);
	while (now < 8 * 3600 * 2) {
		delay(500);
		Serial.print(".");
		now = time(nullptr);
	}
	#ifdef _ARDUINO_
		Serial.println("");
	#endif
	struct tm timeinfo;
	gmtime_r(&now, &timeinfo);
	#ifdef _ARDUINO_
		Serial.print("Current time: ");
	#endif
	Serial.println(asctime(&timeinfo));
}


#ifdef _ESP8266_
void fetchURL(BearSSL::WiFiClientSecure *client, const char *host, const uint16_t port, const char *path) 
{
	if (!path) path = "/";

	Serial.printf("Trying: %s:11884...", host);
	client->connect(host, port);
	if (!client->connected()) 
	{
		Serial.printf("*** Can't connect. ***\n-------\n");
		return;
	}
	Serial.printf("at fetchURL -> Connected!\n-------\n");
	client->stop();
}	

void fetchCertAuthority() 
{
  	#ifdef _ESP8266_
	net.setSession(&session);
	delay(100);
	net.setTrustAnchors(&cert);
	
  	Serial.println("Try again after setting NTP time (should pass)");
  	#endif
  	setClock();
  	delay(500);
}
#endif

int damda_Req_RegisterDevice(){
		
    int rc;
    int ch;
    char topic[100] = "";
    int i=0;
    char* result;
    json_t *root, *msg, *eArray,*eJsonObject1,*eJsonObject2,*eJsonObject3,*eJsonObject4,*eJsonObject5,*eJsonObject6,*eJsonObject7;
	json_t *sidJson,*didJson,*sequenceJson,*operationJson;
	json_t *nJson1,*nJson2,*nJson3,*nJson4,*nJson5,*nJson6,*nJson7;
	json_t *svJson1,*svJson2,*svJson3,*svJson4,*svJson5,*svJson6,*svJson7;

    char sequenceString[10];
    int resultSequence;
    char* n[7] = {"/3/0/0","/3/0/1","/3/0/2","/3/0/3","/3/0/997","/3/0/998","/3/0/999"};

	if(!client.connected())
	{
		#ifdef _DEBUG_
			#ifdef _ARDUINO_
				Serial.println("[damda] Error : Client is not connected.\n");
			#elif _LINUX_
				printf("[damda] Error : Client is not connected.\n");
			#endif
		#endif
		return -7;
	}
    root = json_object();
    msg = json_object();
    eArray = json_array();

    sprintf(topic,"%s/sync/%s/iot-server/register/json",damda_deviceType,deviceId);

    sequence = sequence % MAX_SEQUENCE + 1;
    sprintf(sequenceString,"%d",sequence);
	sidJson = json_string("0");
	didJson = json_string(deviceId);
	sequenceJson = json_string(sequenceString);
	operationJson = json_string("rg");
    json_object_set(root,"sid",sidJson);
    json_object_set(root,"did",didJson);
    json_object_set(root,"seq",sequenceJson);
    json_object_set(msg,"o",operationJson);

    eJsonObject1 = json_object();
	nJson1 = json_string(n[0]);
	svJson1 = json_string(deviceStatus.manufacturer);
    json_object_set(eJsonObject1,"n",nJson1);
    json_object_set(eJsonObject1,"sv",svJson1);
    json_array_append_new(eArray,eJsonObject1);

    eJsonObject2 = json_object();
	nJson2 = json_string(n[1]);
	svJson2 = json_string(deviceStatus.model);
    json_object_set(eJsonObject2,"n",nJson2);
    json_object_set(eJsonObject2,"sv",svJson2);
    json_array_append_new(eArray,eJsonObject2);

    eJsonObject3 = json_object();
	nJson3 = json_string(n[2]);
	svJson3 = json_string(deviceStatus.serial);
    json_object_set(eJsonObject3,"n",nJson3);
    json_object_set(eJsonObject3,"sv",svJson3);
    json_array_append_new(eArray,eJsonObject3);

    eJsonObject4 = json_object();
	nJson4 = json_string(n[3]);
	svJson4 = json_string(deviceStatus.firmwareVersion);
    json_object_set(eJsonObject4,"n",nJson4);
    json_object_set(eJsonObject4,"sv",svJson4);
    json_array_append_new(eArray,eJsonObject4);

    eJsonObject5 = json_object();
	nJson5 = json_string(n[4]);
	svJson5 = json_string(TOPIC_VERSION);
    json_object_set(eJsonObject5,"n",nJson5);
    json_object_set(eJsonObject5,"sv",svJson5);
    json_array_append_new(eArray,eJsonObject5);

    eJsonObject6 = json_object();
	nJson6 = json_string(n[5]);
	svJson6= json_string(PROFILE_VERSION);
    json_object_set(eJsonObject6,"n",nJson6);
    json_object_set(eJsonObject6,"sv",svJson6);
    json_array_append_new(eArray,eJsonObject6);

    eJsonObject7 = json_object();
	nJson7 = json_string(n[6]);
	svJson7= json_string(SERVICE_PROTOCOL_VERSION);
    json_object_set(eJsonObject7,"n",nJson7);
    json_object_set(eJsonObject7,"sv",svJson7);
    json_array_append_new(eArray,eJsonObject7);

    json_object_set(msg,"e",eArray);
    json_object_set(root,"msg",msg);

    result = json_dumps(root,0);

	bool result_ok = client.publish(topic, result);

	#ifdef _DEBUG_
		Serial.print("[damda] Info : topic - ");
		Serial.println(topic);
		Serial.print("[damda] Info : sendMessage - ");
		Serial.println(result);
	#endif

	free(result);
	json_decref(nJson1);
	json_decref(nJson2);
	json_decref(nJson3);
	json_decref(nJson4);
	json_decref(nJson5);
	json_decref(nJson6);
	json_decref(nJson7);
	json_decref(svJson1);
	json_decref(svJson2);
	json_decref(svJson3);
	json_decref(svJson4);
	json_decref(svJson5);
	json_decref(svJson6);
	json_decref(svJson7);
	json_decref(eJsonObject1);
	json_decref(eJsonObject2);
	json_decref(eJsonObject3);
	json_decref(eJsonObject4);
	json_decref(eJsonObject5);
	json_decref(eJsonObject6);
	json_decref(eJsonObject7);
	json_decref(sidJson);
	json_decref(didJson);
	json_decref(sequenceJson);
	json_decref(operationJson);
	json_decref(root);
	json_decref(msg);
	json_decref(eArray);

    resultSequence = sequence;
	Serial.print("[damda] Send OK ");
    return resultSequence;
   
}


int damda_Req_DeregisterDevice(){
    int rc;
    int ch;
    char topic[100] = "";
    int i=0;
    char* result;

    #ifdef _ARDUINO_
    #elif _LINUX_
    	char* copyDeviceId = malloc(sizeof(deviceId)+1);
    #endif

    char* ptr;
    json_t *root, *msg, *eArray,*eJsonObject1,*eJsonObject2,*eJsonObject3;
	json_t *sidJson,*didJson,*sequenceJson,*operationJson;
	json_t *nJson1,*nJson2,*nJson3;
	json_t *svJson1,*svJson2,*svJson3;

    char sequenceString[10];
    int resultSequence;
    char* n[7] = {"/3/0/0","/3/0/1","/3/0/2"};

    #ifdef _ARDUINO_
	char* sv[7] = {deviceStatus.manufacturer,deviceStatus.model,deviceStatus.serial};
    #endif

	if(!client.connected()){
		#ifdef _DEBUG_
			#ifdef _ARDUINO_
				Serial.println("[damda] Error : Client is not connected.\n");
			#elif _LINUX_
				printf("[damda] Error : Client is not connected.\n");
			#endif
		#endif
		return -7;
	}
	root = json_object();
	msg = json_object();
	eArray = json_array();

	#ifdef _ARDUINO_
    #elif _LINUX_
    	strcpy(copyDeviceId,deviceId);
    	ptr = strtok(copyDeviceId,"-");
    #endif

    sprintf(topic,"%s/sync/%s/iot-server/unregister/json",damda_deviceType,deviceId);

    sequence = sequence % MAX_SEQUENCE + 1;
    sprintf(sequenceString,"%d",sequence);
	sidJson = json_string("0");
	didJson = json_string(deviceId);
	sequenceJson = json_string(sequenceString);
	operationJson = json_string("urg");
    json_object_set(root,"sid",sidJson);
    json_object_set(root,"did",didJson);
    json_object_set(root,"seq",sequenceJson);
    json_object_set(msg,"o",operationJson);

	eJsonObject1 = json_object();
	nJson1 = json_string(n[0]);
	#ifdef _ARDUINO_
	svJson1 = json_string(sv[0]);
    #elif _LINUX_
	svJson1 = json_string(ptr);
	#endif
    json_object_set(eJsonObject1,"n",nJson1);
    json_object_set(eJsonObject1,"sv",svJson1);
    json_array_append_new(eArray,eJsonObject1);
    #ifdef _LINUX_
    ptr = strtok(NULL,"-");
	#endif


	eJsonObject2 = json_object();
	nJson2 = json_string(n[1]);
	#ifdef _ARDUINO_
	svJson2 = json_string(sv[1]);
    #elif _LINUX_
	svJson2 = json_string(ptr);
	#endif
    json_object_set(eJsonObject2,"n",nJson2);
    json_object_set(eJsonObject2,"sv",svJson2);
    json_array_append_new(eArray,eJsonObject2);
    #ifdef _LINUX_
    ptr = strtok(NULL,"-");
	#endif

	eJsonObject3 = json_object();
	nJson3 = json_string(n[2]);
	#ifdef _ARDUINO_
	svJson3 = json_string(sv[2]);
    #elif _LINUX_
	svJson3 = json_string(ptr);
	#endif
    json_object_set(eJsonObject3,"n",nJson3);
    json_object_set(eJsonObject3,"sv",svJson3);
    json_array_append_new(eArray,eJsonObject3);
    #ifdef _LINUX_
    ptr = strtok(NULL,"-");
	#endif

	json_object_set(msg,"e",eArray);
    json_object_set(root,"msg",msg);

    result = json_dumps(root,0);

	bool result_ok = client.publish(topic, result);
    //Serial.println(result_ok);

	#ifdef _DEBUG_
		Serial.print("[damda] Info : topic - ");
		Serial.println(topic);
		Serial.print("[damda] Info : sendMessage - ");
		Serial.println(result);
	#endif

	#ifdef _ARDUINO_
    #elif _LINUX_
    free(copyDeviceId);
    #endif

    free(result);
	json_decref(eJsonObject1);
	json_decref(eJsonObject2);
	json_decref(eJsonObject3);
	json_decref(nJson1);
	json_decref(nJson2);
	json_decref(nJson3);
	json_decref(svJson1);
	json_decref(svJson2);
	json_decref(svJson3);
	json_decref(sidJson);
	json_decref(didJson);
	json_decref(sequenceJson);
	json_decref(operationJson);

	json_decref(root);
	json_decref(msg);
	json_decref(eArray);

    resultSequence = sequence;
  	return resultSequence;
}


int damda_Req_DeleteDevice(){
    int rc;
    int ch;
    char topic[100] = "";
    int i=0;
    char* result;

    #ifdef _ARDUINO_
    #elif _LINUX_
    	char* copyDeviceId = malloc(sizeof(deviceId)+1);
    #endif

    char* ptr;
    json_t *root, *msg, *eArray,*eJsonObject1,*eJsonObject2,*eJsonObject3;
	json_t *sidJson,*didJson,*sequenceJson,*operationJson;
	json_t *nJson1,*nJson2,*nJson3;
	json_t *svJson1,*svJson2,*svJson3;

    char sequenceString[10];

    int resultSequence;
    char* n[7] = {"/3/0/0","/3/0/1","/3/0/2"};

    #ifdef _ARDUINO_
	char* sv[7] = {deviceStatus.manufacturer,deviceStatus.model,deviceStatus.serial};
    #endif

	if(!client.connected()){
		#ifdef _DEBUG_
			#ifdef _ARDUINO_
				Serial.println("[damda] Error : Client is not connected.\n");
			#elif _LINUX_
				printf("[damda] Error : Client is not connected.\n");
			#endif
		#endif
		return -7;
	}

	root = json_object();
	msg = json_object();
	eArray = json_array();

	#ifdef _ARDUINO_
    #elif _LINUX_
    	strcpy(copyDeviceId,deviceId);
    	ptr = strtok(copyDeviceId,"-");
    #endif

    sprintf(topic,"%s/sync/%s/iot-server/deregister/json",damda_deviceType,deviceId);

    sequence = sequence % MAX_SEQUENCE + 1;
    sprintf(sequenceString,"%d",sequence);

	sidJson = json_string("0");
	didJson = json_string(deviceId);
	sequenceJson = json_string(sequenceString);
	operationJson = json_string("drg");
    json_object_set(root,"sid",sidJson);
    json_object_set(root,"did",didJson);
    json_object_set(root,"seq",sequenceJson);
    json_object_set(msg,"o",operationJson);

	eJsonObject1 = json_object();
	nJson1 = json_string(n[0]);
	#ifdef _ARDUINO_
	svJson1 = json_string(sv[0]);
    #elif _LINUX_
	svJson1 = json_string(ptr);
	#endif
    json_object_set(eJsonObject1,"n",nJson1);
    json_object_set(eJsonObject1,"sv",svJson1);
    json_array_append_new(eArray,eJsonObject1);
    #ifdef _LINUX_
    ptr = strtok(NULL,"-");
	#endif

	eJsonObject2 = json_object();
	nJson2 = json_string(n[1]);
	#ifdef _ARDUINO_
	svJson2 = json_string(sv[1]);
    #elif _LINUX_
	svJson2 = json_string(ptr);
	#endif
    json_object_set(eJsonObject2,"n",nJson2);
    json_object_set(eJsonObject2,"sv",svJson2);
    json_array_append_new(eArray,eJsonObject2);
    #ifdef _LINUX_
    ptr = strtok(NULL,"-");
	#endif

	eJsonObject3 = json_object();
	nJson3 = json_string(n[2]);
	#ifdef _ARDUINO_
	svJson3 = json_string(sv[2]);
    #elif _LINUX_
	svJson3 = json_string(ptr);
	#endif
    json_object_set(eJsonObject3,"n",nJson3);
    json_object_set(eJsonObject3,"sv",svJson3);
    json_array_append_new(eArray,eJsonObject3);
    #ifdef _LINUX_
    ptr = strtok(NULL,"-");
	#endif

    json_object_set(msg,"e",eArray);
    json_object_set(root,"msg",msg);

    result = json_dumps(root,0);

	bool result_ok = client.publish(topic, result);
    //Serial.println(result_ok);

	#ifdef _DEBUG_
		#ifdef _ARDUINO_
			Serial.print("[damda] Info : topic - ");
			Serial.println(topic);
			Serial.print("[damda] Info : sendMessage - ");
			Serial.println(result);
		#elif _LINUX_
			printf("###########Send Message#############\n");
			printf("[damda] Info : topic - %s\n",topic);
			printf("[damda] Info : sendMessage - %s\n",result);
			printf("####################################\n\n");
		#endif

	#endif

	#ifdef _ARDUINO_
    #elif _LINUX_
    	free(copyDeviceId);
    #endif

    free(result);
	json_decref(eJsonObject1);
	json_decref(eJsonObject2);
	json_decref(eJsonObject3);
	json_decref(nJson1);
	json_decref(nJson2);
	json_decref(nJson3);
	json_decref(svJson1);
	json_decref(svJson2);
	json_decref(svJson3);
	json_decref(sidJson);
	json_decref(didJson);
	json_decref(sequenceJson);
	json_decref(operationJson);
	json_decref(root);
	json_decref(msg);
	json_decref(eArray);

    resultSequence = sequence;
    return resultSequence;
}


int damda_Req_UpdateDeviceInfo(){
    int rc;
    int ch;
    char topic[100] = "";
    int i=0;
    char* result;
    json_t *root, *msg, *eArray,*eJsonObject1,*eJsonObject2,*eJsonObject3,*eJsonObject4,*eJsonObject5,*eJsonObject6,*eJsonObject7;
	json_t *sidJson,*didJson,*sequenceJson,*operationJson;
	json_t *nJson1,*nJson2,*nJson3,*nJson4,*nJson5,*nJson6,*nJson7;
	json_t *svJson1,*svJson2,*svJson3,*svJson4,*svJson5,*svJson6,*svJson7;

    char sequenceString[10];
    int resultSequence;
    char* n[7] = {"/3/0/0","/3/0/1","/3/0/2","/3/0/3","/3/0/997","/3/0/998","/3/0/999"};

	if(!client.connected()){
		#ifdef _DEBUG_
			#ifdef _ARDUINO_
				Serial.println("[damda] Error : Client is not connected.");
			#elif _LINUX_
				printf("[damda] Error : Client is not connected.\n");
			#endif
		#endif
		return -7;
	}
	
    root = json_object();
    msg = json_object();
    eArray = json_array();
    sequence = sequence % MAX_SEQUENCE + 1;
    if(sequence != NULL){
    	sprintf(sequenceString,"%d",sequence);
	}
	if(deviceId != NULL){
	    sprintf(topic,"%s/sync/%s/iot-server/update/json",damda_deviceType,deviceId);
	}
	
	sidJson = json_string("3");
	didJson = json_string(deviceId);
	sequenceJson = json_string(sequenceString);
	operationJson = json_string("up");
    json_object_set(root,"sid",sidJson);
    json_object_set(root,"did",didJson);
    json_object_set(root,"seq",sequenceJson);
    json_object_set(msg,"o",operationJson);
	
    eJsonObject1 = json_object();
	nJson1 = json_string(n[0]);
	svJson1 = json_string(deviceStatus.manufacturer);
    json_object_set(eJsonObject1,"n",nJson1);
    json_object_set(eJsonObject1,"sv",svJson1);
    json_array_append_new(eArray,eJsonObject1);

    eJsonObject2 = json_object();
	nJson2 = json_string(n[1]);
	svJson2 = json_string(deviceStatus.model);
    json_object_set(eJsonObject2,"n",nJson2);
    json_object_set(eJsonObject2,"sv",svJson2);
    json_array_append_new(eArray,eJsonObject2);


    eJsonObject3 = json_object();
	nJson3 = json_string(n[2]);
	svJson3 = json_string(deviceStatus.serial);
    json_object_set(eJsonObject3,"n",nJson3);
    json_object_set(eJsonObject3,"sv",svJson3);
    json_array_append_new(eArray,eJsonObject3);


    eJsonObject4 = json_object();
	nJson4 = json_string(n[3]);
	svJson4 = json_string(deviceStatus.firmwareVersion);
    json_object_set(eJsonObject4,"n",nJson4);
    json_object_set(eJsonObject4,"sv",svJson4);
    json_array_append_new(eArray,eJsonObject4);


    eJsonObject5 = json_object();
	nJson5 = json_string(n[4]);
	svJson5 = json_string(TOPIC_VERSION);
    json_object_set(eJsonObject5,"n",nJson5);
    json_object_set(eJsonObject5,"sv",svJson5);
    json_array_append_new(eArray,eJsonObject5);


    eJsonObject6 = json_object();
	nJson6 = json_string(n[5]);
	svJson6= json_string(PROFILE_VERSION);
    json_object_set(eJsonObject6,"n",nJson6);
    json_object_set(eJsonObject6,"sv",svJson6);
    json_array_append_new(eArray,eJsonObject6);


    eJsonObject7 = json_object();
	nJson7 = json_string(n[6]);
	svJson7= json_string(SERVICE_PROTOCOL_VERSION);
    json_object_set(eJsonObject7,"n",nJson7);
    json_object_set(eJsonObject7,"sv",svJson7);
    json_array_append_new(eArray,eJsonObject7);

    json_object_set(msg,"e",eArray);
    json_object_set(root,"msg",msg);

    result = json_dumps(root,0);

	bool result_ok = client.publish(topic, result);
    //Serial.println(result_ok);
	#ifdef _DEBUG_
		#ifdef _ARDUINO_
			Serial.print("[damda] Info : topic - ");
			Serial.println(topic);
			Serial.print("[damda] Info : sendMessage - ");
			Serial.println(result);
		#elif _LINUX_
			printf("###########Send Message#############\n");
			printf("[damda] Info : topic - %s\n",topic);
			printf("[damda] Info : sendMessage - %s\n",result);
			printf("####################################\n\n");
		#endif

	#endif
   	free(result);
	json_decref(nJson1);
	json_decref(nJson2);
	json_decref(nJson3);
	json_decref(nJson4);
	json_decref(nJson5);
	json_decref(nJson6);
	json_decref(nJson7);
	json_decref(svJson1);
	json_decref(svJson2);
	json_decref(svJson3);
	json_decref(svJson4);
	json_decref(svJson5);
	json_decref(svJson6);
	json_decref(svJson7);
	json_decref(eJsonObject1);
	json_decref(eJsonObject2);
	json_decref(eJsonObject3);
	json_decref(eJsonObject4);
	json_decref(eJsonObject5);
	json_decref(eJsonObject6);
	json_decref(eJsonObject7);
	json_decref(sidJson);
	json_decref(didJson);
	json_decref(sequenceJson);
	json_decref(operationJson);
	json_decref(root);
	json_decref(msg);
	json_decref(eArray);

    resultSequence = sequence;
    return resultSequence;
}


int damda_Req_SeverTimeInfo(){
    int rc;
    int ch;
    char topic[100] = "";
    char* result;
    json_t *root, *msg;
	json_t *sidJson,*didJson,*sequenceJson,*operationJson;
    char sequenceString[10];

    int resultSequence;

	if(!client.connected()){
		#ifdef _DEBUG_
			#ifdef _ARDUINO_
				Serial.println("[damda] Error : Client is not connected.");
			#elif _LINUX_
				printf("[damda] Error : Client is not connected.\n");
			#endif

		#endif
		return -7;
	}

    root = json_object();
    msg = json_object();

    sprintf(topic,"%s/sync/%s/iot-server/timesync/json",damda_deviceType,deviceId);

    sequence = sequence % MAX_SEQUENCE + 1;
    sprintf(sequenceString,"%d",sequence);
	sidJson = json_string("0");
	didJson = json_string(deviceId);
	sequenceJson = json_string(sequenceString);
	operationJson = json_string("tisyn");

    json_object_set(root,"sid",sidJson);
    json_object_set(root,"did",didJson);
    json_object_set(root,"seq",sequenceJson);
    json_object_set(msg,"o",operationJson);
    json_object_set(root,"msg",msg);

    result = json_dumps(root,0);

	bool result_ok = client.publish(topic, result);
    //Serial.println(result_ok);

	#ifdef _DEBUG_
		#ifdef _ARDUINO_
			Serial.print("[damda] Info : topic - ");
			Serial.println(topic);
			Serial.print("[damda] Info : sendMessage - ");
			Serial.println(result);
		#elif _LINUX_
			printf("###########Send Message#############\n");
			printf("[damda] Info : topic - %s\n",topic);
			printf("[damda] Info : sendMessage - %s\n",result);
			printf("####################################\n\n");
		#endif

	#endif

    free(result);
	json_decref(sidJson);
	json_decref(didJson);
	json_decref(sequenceJson);
	json_decref(operationJson);
	json_decref(root);
	json_decref(msg);

    resultSequence = sequence;
    return resultSequence;
}


int damda_Notify_DeviceStatus(const char* notificationType,NotifyParameter notifyParams[],int arrayCount){
    int rc;
    int ch;
    char topic[100] = "";
    int i=0;
    char* result;
	const int cArrayCount = arrayCount;
    json_t *root, *msg, *eArray;
	json_t* eJsonObject[cArrayCount];
	json_t* nJson[cArrayCount];
	json_t* svJson[cArrayCount];
	json_t* tiJson[cArrayCount];
	json_t *sidJson,*didJson,*sequenceJson,*operationJson;
    char sequenceString[10];
    int resultSequence;
	
	if(!client.connected()){
		#ifdef _DEBUG_
			#ifdef _ARDUINO_
				Serial.println("[damda] Error : Client is not connected.");
			#elif _LINUX_
				printf("[damda] Error : Client is not connected.\n");
			#endif
		#endif
		return -7;
	}
    root = json_object();
    msg = json_object();
    eArray = json_array();
    
	sprintf(topic,"%s/sync/%s/iot-server/notify/json",damda_deviceType,deviceId);

    sequence = sequence % MAX_SEQUENCE + 1;
    sprintf(sequenceString,"%d",sequence);
	sidJson = json_string(notificationType);
	didJson = json_string(deviceId);
	sequenceJson = json_string(sequenceString);
	operationJson = json_string("n");

    json_object_set(root,"sid",sidJson);
    json_object_set(root,"did",didJson);
    json_object_set(root,"seq",sequenceJson);
    json_object_set(msg,"o",operationJson);
    for(i=0;i<arrayCount;i++){
	    eJsonObject[i] = json_object();
		nJson[i] = json_string(notifyParams[i].resourceUri);
		svJson[i] = json_string(notifyParams[i].stringValue);
		tiJson[i] = json_string(notifyParams[i].time);
	    json_object_set(eJsonObject[i],"n",nJson[i]);
	    json_object_set(eJsonObject[i],"sv",svJson[i]);
	    json_object_set(eJsonObject[i],"ti",tiJson[i]);
	    json_array_append_new(eArray,eJsonObject[i]);
	}

    json_object_set(msg,"e",eArray);
    json_object_set(root,"msg",msg);

    result = json_dumps(root,0);

	bool result_ok = client.publish(topic, result);
    //Serial.println(result_ok);

	#ifdef _DEBUG_
		#ifdef _ARDUINO_
			Serial.print("[damda] Info : topic - ");
			Serial.println(topic);
			Serial.print("[damda] Info : sendMessage - ");
			Serial.println(result);
		#elif _LINUX_
			printf("###########Send Message#############\n");
			printf("[damda] Info : topic - %s\n",topic);
			printf("[damda] Info : sendMessage - %s\n",result);
			printf("####################################\n\n");
		#endif

	#endif

    free(result);
	for(i=0;i<arrayCount;i++){
		json_decref(eJsonObject[i]);
		json_decref(nJson[i]);
		json_decref(svJson[i]);
		json_decref(tiJson[i]);
	}

	json_decref(sidJson);
	json_decref(didJson);
	json_decref(sequenceJson);
	json_decref(operationJson);
	json_decref(root);
	json_decref(msg);
	json_decref(eArray);

    resultSequence = sequence;
    return resultSequence;
}

int damda_Rsp_RemoteControl(int controlType,const char* sid,const char* returnCode,char* resourceUri[],char* stringValue[],int arrayCount){
    int ch;
    char topic[100];
    int i=0;
    char* result;
	const int cArrayCount = arrayCount;
    json_t *root, *msg, *eArray;
	json_t *sidJson,*didJson,*returnCodeJson,*operationJson;
	json_t *eJsonObject[cArrayCount];
	json_t *nJson[cArrayCount];
	json_t *svJson[cArrayCount];

	if(!client.connected()){
		#ifdef _DEBUG_
			#ifdef _ARDUINO_
				Serial.println("[damda] Error : Client is not connected.");
			#elif _LINUX_
				printf("[damda] Error : Client is not connected.\n");
			#endif
		#endif
		return -7;
	}
    root = json_object();
    msg = json_object();
    eArray = json_array();


    if(controlType == 2){
	    sprintf(topic,"%s/sync/%s/iot-server/read/json",damda_deviceType,deviceId);
    }
    else if(controlType == 3){
	    sprintf(topic,"%s/sync/%s/iot-server/write/json",damda_deviceType,deviceId);
    }
    else if(controlType == 4){
		sprintf(topic,"%s/sync/%s/iot-server/execute/json",damda_deviceType,deviceId);
    }
    else {
		printf("[damda] Error : invalid controlType \n");
	return -1;
    }
	sidJson = json_string(sid);
	didJson = json_string(deviceId);
	returnCodeJson = json_string("res");
	operationJson = json_string(returnCode);

    json_object_set(root,"sid",sidJson);
    json_object_set(root,"did",didJson);
    json_object_set(msg,"o",returnCodeJson);
    json_object_set(msg,"rc",operationJson);

	for(i=0;i<arrayCount;i++){
    eJsonObject[i] = json_object();
	nJson[i] = json_string(resourceUri[i]);
	svJson[i] = json_string(stringValue[i]);
    json_object_set(eJsonObject[i],"n",nJson[i]);
    json_object_set(eJsonObject[i],"sv",svJson[i]);
    json_array_append_new(eArray,eJsonObject[i]);
     }
    json_object_set(msg,"e",eArray);
    json_object_set(root,"msg",msg);

    result = json_dumps(root,0);

    bool result_ok = client.publish(topic, result);
    //Serial.println(result_ok);

	#ifdef _DEBUG_
		#ifdef _ARDUINO_
			Serial.print("[damda] Info : topic - ");
			Serial.println(topic);
			Serial.print("[damda] Info : sendMessage - ");
			Serial.println(result);
		#elif _LINUX_
			printf("###########Send Message#############\n");
			printf("[damda] Info : topic - %s\n",topic);
			printf("[damda] Info : sendMessage - %s\n",result);
			printf("####################################\n\n");
		#endif

	#endif

    free(result);
	for(i=0;i<arrayCount;i++){
		json_decref(eJsonObject[i]);
		json_decref(nJson[i]);
		json_decref(svJson[i]);
	}
	json_decref(sidJson);
	json_decref(didJson);
	json_decref(returnCodeJson);  //jake
	json_decref(operationJson);
	json_decref(root);
	json_decref(msg);
	json_decref(eArray);

    return 1;

}


int damda_Rsp_RemoteControl_noEntity(int controlType,const char* sid,const char* returnCode){
    int ch;
    char topic[100];
    int i=0;
    char* result;
    json_t *root, *msg, *eArray;
	json_t *sidJson,*didJson,*returnCodeJson,*operationJson;

	if(!client.connected()){
		#ifdef _DEBUG_
			#ifdef _ARDUINO_
				Serial.println("[damda] Error : Client is not connected.");
			#elif _LINUX_
				printf("[damda] Error : Client is not connected.\n");
			#endif
		#endif
		return -7;
	}

    if(controlType == 2){
	    sprintf(topic,"%s/sync/%s/iot-server/read/json",damda_deviceType,deviceId);
    }
    else if(controlType == 3){
	    sprintf(topic,"%s/sync/%s/iot-server/write/json",damda_deviceType,deviceId);
    }
    else if(controlType == 4){
       	sprintf(topic,"%s/sync/%s/iot-server/execute/json",damda_deviceType,deviceId);
    }
    else if(controlType == 5){
       	sprintf(topic,"%s/sync/%s/iot-server/delete/json",damda_deviceType,deviceId);
    }
    else {
    	#ifdef _ARDUINO_
			Serial.println("invalid controlType\n");
		#elif _LINUX_
			printf("invalid controlType\n");
		#endif
	return -1;
    }
/*
	#ifdef _ARDUINO_
		Serial.print("in Sid :");
		Serial.println(sid);
	#elif _LINUX_
		printf("in Sid : %s\n",sid);
	#endif
*/
    root = json_object();
    msg = json_object();
    eArray = json_array();
	sidJson = json_string(sid);
	didJson = json_string(deviceId);
	operationJson = json_string("res");
	returnCodeJson = json_string(returnCode);

    json_object_set(root,"sid",sidJson);
    json_object_set(root,"did",didJson);
    json_object_set(msg,"o",operationJson);
    json_object_set(msg,"rc",returnCodeJson);
    json_object_set(root,"msg",msg);

    result = json_dumps(root,0);

    bool result_ok = client.publish(topic, result);
    //Serial.println(result_ok);

	#ifdef _DEBUG_
		#ifdef _ARDUINO_
			Serial.print("[damda] Info : topic - ");
			Serial.println(topic);
			Serial.print("[damda] Info : sendMessage - ");
			Serial.println(result);
		#elif _LINUX_
			printf("###########Send Message#############\n");
			printf("[damda] Info : topic - %s\n",topic);
			printf("[damda] Info : sendMessage - %s\n",result);
			printf("####################################\n\n");
		#endif
	#endif

   	free(result);
	json_decref(sidJson);
	json_decref(didJson);
	json_decref(returnCodeJson);
	json_decref(operationJson);
	json_decref(root);
	json_decref(msg);
	json_decref(eArray);

    return 1;
}

int damda_Rsp_RemoteDelDevice(const char* sid, const char* returnCode){
    int rcInt;
    int ch;
    char topic[100] = "";
    int i=0;
    char* result;
    json_t *root, *msg;
	json_t *sidJson,*didJson,*returnCodeJson,*operationJson;

	if(!client.connected()){
		#ifdef _DEBUG_
			#ifdef _ARDUINO_
				Serial.println("[damda] Error : Client is not connected.");
			#elif _LINUX_
				printf("[damda] Error : Client is not connected.\n");
			#endif
		#endif
		return -7;
	}

    root = json_object();
    msg = json_object();
	sidJson = json_string(sid);
	didJson = json_string(deviceId);
	operationJson = json_string("res");
	returnCodeJson = json_string(returnCode);

    sprintf(topic,"%s/sync/%s/iot-server/delete/json",damda_deviceType,deviceId);

    json_object_set(root,"sid",sidJson);
    json_object_set(root,"did",didJson);
    json_object_set(msg,"o",operationJson);
    json_object_set(msg,"rc",returnCodeJson);

    json_object_set(root,"msg",msg);

    result = json_dumps(root,0);
    /*MQTTAsync_message pubmsg = MQTTAsync_message_initializer;
    MQTTAsync_responseOptions opts = MQTTAsync_responseOptions_initializer;
    pubmsg.payload = result;
    pubmsg.payloadlen = (int)strlen(result);
    pubmsg.qos = QOS;
    pubmsg.retained = 0;
    opts.context = client;
	MQTTAsync_sendMessage(client, topic, &pubmsg, &opts);*/

    bool result_ok = client.publish(topic, result);
    //Serial.println(result_ok);

	#ifdef _DEBUG_
		#ifdef _ARDUINO_
			Serial.print("[damda] Info : topic - ");
			Serial.println(topic);
			Serial.print("[damda] Info : sendMessage - ");
			Serial.println(result);
		#elif _LINUX_
			printf("###########Send Message#############\n");
			printf("[damda] Info : topic - %s\n",topic);
			printf("[damda] Info : sendMessage - %s\n",result);
			printf("####################################\n\n");
		#endif
	#endif

    free(result);
	json_decref(sidJson);
	json_decref(didJson);

	json_decref(operationJson);
	json_decref(root);
	json_decref(msg);

    return 1;
}


char* damda_getSDKVersion()
{
	return DAMDA_SDK_VERSION;
}


int damda_isReceiveServerInfo()
{
	return receiveServerInfoStatus;
}


DAMDA_connectionInfo getConnectionInfo()
{
	return damda_connInfo;
}


DeviceStatusInfo getDeviceInfo(){
	return deviceStatus;
}


void damda_setupDeviceInfo(char* manufacturer,char* model,char* serial,char* firmwareVersion,char* deviceType){
	deviceStatus.manufacturer = manufacturer;
	deviceStatus.model = model;
	deviceStatus.serial = serial;
	deviceStatus.firmwareVersion = firmwareVersion;
	sprintf(deviceId,"%s-%s-%s",deviceStatus.manufacturer,deviceStatus.model,deviceStatus.serial);
	strcpy(damda_deviceType,deviceType);
}

char *httpParser(char *s, const char *olds, const char *news) {
	char *result, *sr;
	size_t i, count = 0;
	size_t oldlen = strlen(olds); if (oldlen < 1) return s;
	size_t newlen = strlen(news);

	if (newlen != oldlen) {
		for (i = 0; s[i] != '\0';) {
      		if (memcmp(&s[i], olds, oldlen) == 0) count++, i += oldlen;
      		else i++;
    	}
  	}
  	else i = strlen(s);

  	result = (char *) malloc(i + 1 + count * (newlen - oldlen));
  	if (result == NULL) return NULL;

  	sr = result;
  	while (*s) {
    	if (memcmp(s, olds, oldlen) == 0) {
      		memcpy(sr, news, newlen);
      		sr += newlen;
      		s  += oldlen;
    	}
    	else *sr++ = *s++;
  	}
  	*sr = '\0';

  	return result;
}


int getSid(char* message,char* mSid){
	json_error_t jsonError;
	json_t* messageJson = json_loads(message,0,&jsonError);
	
	char* sid = (char*)json_string_value(json_object_get(messageJson,"sid"));
	
	char copySid[100];
	if(sid != NULL){
		strcpy(mSid,sid);
		json_decref(messageJson);
		return 1;
	} 
	else {
		json_decref(messageJson);
		return 0;
	}
}

/*
 *brief : get seq of response
 *param(char*) : message
 *return : seq
 */
int getSeq(char* message,int* mSeq){
	json_error_t jsonError;
	json_t* messageJson = json_loads(message,0,&jsonError);
	int seqInteger;
	
	char* seq = (char*)json_string_value(json_object_get(messageJson,"seq"));
	
	if(seq!= NULL){
		*mSeq = atoi(seq);
		json_decref(messageJson);
		return *mSeq;
	} 
	else {
		json_decref(messageJson);
		return -1;
	}
}

/*
 *brief : get operation of response
 *param(char*) : message
 *return : operation
 */
int getOperation(char* message,char* operation)
{
	json_error_t jsonError;
	json_t* messageJson;
	json_t* jsonMsg;

	messageJson =  json_loads(message,0,&jsonError);
	jsonMsg = json_object_get(messageJson,"msg");
	char* o = (char*)json_string_value(json_object_get(jsonMsg,"o"));
	if(o!=NULL)
	{
		strcpy(operation,o);
		json_decref(messageJson);
		return 1;
	} 
	else {
		json_decref(messageJson);
		return 0;
	}
}

/*
 *brief : get returnCode of reponse
 *param(char*) : message
 *return : returnCode
 */
int getReturnCode(char* message,int* mReturnCode)
{
	json_error_t jsonError;
	json_t* messageJson = json_loads(message,0,&jsonError);
	json_t* jsonMsg = json_object_get(messageJson,"msg");
		
    char* rc = (char*)json_string_value(json_object_get(jsonMsg,"rc")); 
	
	if(rc != NULL)
	{
		*mReturnCode = atoi(rc);			
		json_decref(messageJson);
		return *mReturnCode;
	} else {
		json_decref(messageJson);
		return -1;
	}
}

/*
 *brief : get errorCode of reponse
 *param(char*) : message
 *return : errorCode
 */
int getErrorCode(char* message,int* mErrorCode)
{
	json_error_t jsonError;
	json_t* messageJson = json_loads(message,0,&jsonError);
	json_t* jsonMsg = json_object_get(messageJson,"msg");
	
    char* ec = (char*)json_string_value(json_object_get(jsonMsg,"ec"));
	if(ec != NULL){
		*mErrorCode = atoi(ec);
		json_decref(messageJson);
		return *mErrorCode;
	} else {
		json_decref(messageJson);
		return -1;
	}
}

/*
 *brief : get resourceUri of reponse
 *param(char*) : message
 *param(size_t) : index
 *return : resourceUri
 */
int getResourceUri(char* message,size_t index,char* mResourceUri)
{
	json_error_t jsonError;
	json_t* messageJson;
	json_t* jsonMsg; 
	json_t* eArray;
	json_t* eJsonObject;

    messageJson = json_loads(message,0,&jsonError);
	jsonMsg = json_object_get(messageJson,"msg");
	eArray = json_object_get(jsonMsg,"e");
	eJsonObject = json_array_get(eArray,index);
    char* n = (char*)json_string_value(json_object_get(eJsonObject,"n"));
	if(n != NULL){
		strcpy(mResourceUri,n);
		json_decref(messageJson);
		return 1;
	}
	else {
		json_decref(messageJson);
		return 0;
	}
}

/*
 *brief : get entity array size of reponse
 *param(char *) : message
 *return : entity array size
 */
int getNArraySize(char* message)
{
	json_error_t jsonError;
	json_t* messageJson;
	json_t* jsonMsg;
	json_t* eArray;
	size_t jsonArraySize;
	
	int i=0;
	messageJson = json_loads(message,0,&jsonError);
	jsonMsg = json_object_get(messageJson,"msg");
	eArray = json_object_get(jsonMsg,"e");
	jsonArraySize = json_array_size(eArray);
	json_decref(messageJson);
		
	return jsonArraySize;
}


/*
 *brief : get stringValue of reponse
 *param(char *) : message
 *param(size_t) : index
 *return : stringValue
 */
int getStringValue(char* message,size_t index,char* mStringValue)
{
	json_error_t jsonError;
	json_t* messageJson ;
	json_t* jsonMsg ;
	json_t* eArray ;
	json_t* eJsonObject ;
	
	messageJson = json_loads(message,0,&jsonError);
	jsonMsg = json_object_get(messageJson,"msg");
	eArray = json_object_get(jsonMsg,"e");
	eJsonObject = json_array_get(eArray,index);
    char* sv = (char*)json_string_value(json_object_get(eJsonObject,"sv"));
	if(sv != NULL)
	{
		strcpy(mStringValue,sv);
		json_decref(messageJson);
		return 1;
	} 
	else
	{
		json_decref(messageJson);
		return 0;
	}
}

/*
 *brief : get time of response
 *param(char *) : message
 *param(size_t) : index
 *return : time
 */
int getTime(char* message,size_t index,char* mTime)
{
	json_error_t jsonError;
	json_t* messageJson = json_loads(message,0,&jsonError);
	json_t* jsonMsg = json_object_get(messageJson,"msg");
	json_t* eArray = json_object_get(jsonMsg,"e");
	json_t* eJsonObject = json_array_get(eArray,index);

	char* ti = (char*)json_string_value(json_object_get(eJsonObject,"ti"));
	if(ti != NULL)
	{
		strcpy(mTime,ti);
		json_decref(messageJson);
		return 1;
	} 
	else
	{
		json_decref(messageJson);
		return 0;
	}    
}

/*
 *brief : get serviceId of response
 *param(char*) : topic
 *return : serviceId
 */
char* getServiceId(char* topic)
{
	char tempTopicService[100];
	strcpy(tempTopicService,topic);
	char* ptrServiceId = strtok(tempTopicService,"/");
	return ptrServiceId;
}

/*
 *brief : check firmwareUpdateMessage
 *param(char*) : message
 *return : firmwareUpdateMessage 1, not firmwareUpdateMessage 0
 */
int isFirmwareUpdateMessage(char* message)
{
	json_error_t jsonError;
	json_t* messageJson = json_loads(message,0,&jsonError);
	json_t* jsonMsg = json_object_get(messageJson,"msg");
	json_t* eArray = json_object_get(jsonMsg,"e");
	json_t* eJsonObject;
	int jsonArraySize = json_array_size(eArray);
	int i = 0;
	int updateNCount = 0;
	char nValue[15];
	if(jsonArraySize == 0)
	{
		json_decref(messageJson);
		return 0;
	}
	for(i=0; i<jsonArraySize; i++)
	{
		eJsonObject = json_array_get(eArray,i);
		strcpy(nValue,json_string_value(json_object_get(eJsonObject,"n")));
		if(strcmp(nValue,"/3/0/3") == 0 || strcmp(nValue,"/5/0/1") == 0)
		{
			updateNCount++;
		}
	}
	json_decref(messageJson);
	if(updateNCount == 2) return 1;
	else return 0;	
}

/*
 *brief : get stringValue of reponse by resourceUri
 *param(char*) : message
 *param(char*) : n
 *return : stringValue
 */
int getStringValueByResourceUri(char* message,char* n,char* mStringValue)
{
	json_error_t jsonError;
	json_t* messageJson = json_loads(message,0,&jsonError);
	json_t* jsonMsg = json_object_get(messageJson,"msg");
	json_t* eArray = json_object_get(jsonMsg,"e");
	json_t* eJsonObject;
	
	int jsonArraySize = json_array_size(eArray);
	int i=0;
	char nValue[15];
	
	if(jsonArraySize==0)
	{
		json_decref(messageJson);
		return 0;
	}
	for(i=0;i<jsonArraySize;i++)
	{
		eJsonObject = json_array_get(eArray,i);
		strcpy(nValue,json_string_value(json_object_get(eJsonObject,"n")));
		
		if(strcmp(nValue,n)==0)
		{
			char* sv = (char*)json_string_value(json_object_get(eJsonObject,"sv"));
			if(sv != NULL)
			{
				strcpy(mStringValue,sv);
				json_decref(messageJson);
				return 1;
			} else 
			{
				json_decref(messageJson);
				return 0;
			}
		}
	}
	json_decref(messageJson);
	return 0;
}

/*
 *brief : get messageType of response
 *param(char*) : topic
 *return : messageType
 */
void getMsgType(char* topic,char* mMsgType)
{
	int i=0;
	char tempTopicMsgType[100];
	strcpy(tempTopicMsgType,topic);
	
	char* ptrMsgType = strtok(tempTopicMsgType,"/");
	
	while(ptrMsgType!=NULL)
	{		
		ptrMsgType = strtok(NULL,"/");
		if(i == 0) break;
		i++;
	}
	strcpy(mMsgType,ptrMsgType);
	//free(ptrMsgType);	
}

/*
 *brief : get originId of response
 *param(char*) : topic
 *return : originId
 */
void getOriginId(char* topic,char* mOriginId)
{
	int i = 0;
	char tempTopicOriginId[100];
	strcpy(tempTopicOriginId,topic);
	
	char* ptrOriginId = strtok(tempTopicOriginId,"/");
			
	while(ptrOriginId != NULL)
	{
		ptrOriginId = strtok(NULL,"/");
		if(i==1) break;
		i++;
	}
	strcpy(mOriginId,ptrOriginId);        
}

/*
 *brief : get targetId of response
 *param(char*) : topic
 *return : targetId
 */
void getTargetId(char* topic,char* mTargetId)
{
	int i=0;
	char tempTopicTargetId[100];
	strcpy(tempTopicTargetId,topic);

	char* ptrTargetId = strtok(tempTopicTargetId,"/");
	
	while(ptrTargetId!=NULL)
	{
		ptrTargetId = strtok(NULL,"/");
		if(i==2) break;
		i++;
	}
	strcpy(mTargetId,ptrTargetId);
}

/*
 *brief : get topic operation of response
 *param(char*) : topic
 *return topic operation
 */
void getTopicOprId(char* topic,char* mTopicOprId)
{
	int i=0;
	char tempTopicOperationId[100];
	strcpy(tempTopicOperationId,topic);
	char* ptrOperationId = strtok(tempTopicOperationId,"/");    
			
	while(ptrOperationId!=NULL)
	{
		ptrOperationId = strtok(NULL,"/");
		if(i==3) break;
		i++;
	}
	strcpy(mTopicOprId,ptrOperationId);
}


int getMessageType(char* topic,char* message)
{
	char operation[10];
	char msgType[10];
	getOperation(message,operation);
	getMsgType(topic,msgType);
	if(isFirmwareUpdateMessage(message))
	{
		return MESSAGE_TYPE_FIRMWARE_DOWNLOAD;
	}
	else if(strcmp(operation,"res")==0)
	{
		return MESSAGE_TYPE_RESPONSE;
	}
	else if(strcmp(operation,"r")==0)
	{
		if(strcmp(msgType,"sync")==0)
		{
			return MESSAGE_TYPE_READ;
		} 
		else if(strcmp(msgType,"async")==0)
		{
			return MESSAGE_TYPE_READ_ASYNC;
		}
	}
	else if(strcmp(operation,"w")==0)
	{
		if(strcmp(msgType,"sync")==0)
		{
			return MESSAGE_TYPE_WRITE;
		} else if(strcmp(msgType,"async")==0)
		{
			return MESSAGE_TYPE_WRITE_ASYNC;
		}
	}
	else if(strcmp(operation,"e")==0)
	{
		if(strcmp(msgType,"sync")==0)
		{
			return MESSAGE_TYPE_EXECUTE;
		} 
		else if(strcmp(msgType,"async")==0)
		{
			return MESSAGE_TYPE_EXECUTE_ASYNC;
		}
	}
	else if(strcmp(operation,"d")==0 && strcmp(msgType,"sync") == 0)
	{
		return MESSAGE_TYPE_DELETE_SYNC;
    }
    else if(strcmp(operation,"d")==0 && strcmp(msgType,"async") == 0)
	{
		return MESSAGE_TYPE_DELETE_ASYNC;
	}
}


void messageReceived(MQTTClient *client, char topic[], char payloads[], int length)
{
	char* payload;
	char* topicName = topic;
	int payloadlen = length;
	int messagelen = strlen(payloads);
	const int arr_size= sizeof(char)*payloadlen+sizeof(char);
	char payloadptr[arr_size];
	int nameIdx = 0;
	int jsonArraySize = 0;
	int svIdx = 0;
	int tiIdx = 0;
	int msgType = 0;
	int nSize = 0;
	int svSize = 0;
	int tiSize = 0;
	int freeIndex = 0;
	int urllen = 0;
	int firmwareVersionlen = 0;

	char sid[50];
	int returnCode = 0;
	int errorCode = 0;
	int seq = -1;
	char firmwareDownloadUrl[128];
	char firmwareVersion[15];
	char operationId[10]; 
	char resourceUri[5][20];
	char stringValue[5][30];
	char receiveTime[5][15];
	
	payload = payloads;
	
    if(messagelen != payloadlen){
    	if(payloadlen != NULL){
			strncpy(payloadptr,payload,payloadlen);
		}
		payloadptr[payloadlen] = '\0';
    }
    else {
    	if(payload != NULL){
			strcpy(payloadptr,payload);
		}
    }

	ResponseMessageInfo msgInfo;
	memset(&msgInfo,0x00,sizeof(msgInfo));	

	getSid(payloadptr,sid);
	if(sid != NULL){
		strcpy(msgInfo.sid,sid);
	}

	#ifdef _DEBUG_
		Serial.println(""); 
		Serial.print("Response : ");
		Serial.println(payloadptr); 
	#endif
	
	getReturnCode(payloadptr,&returnCode);
	if(returnCode){
		msgInfo.returnCode = returnCode;
	}
	getErrorCode(payloadptr,&errorCode);
	if(errorCode){
		msgInfo.errorCode =  errorCode;
	}
	getSeq(payloadptr,&seq);
	if(seq != -1){
		msgInfo.seq = seq;
	}
	
	#ifdef _DEBUG_
	/*Serial.print("getReturnCode(payloadptr) : ");
	Serial.println(msgInfo.returnCode);
	Serial.print("getErrorCode(payloadptr) : ");
	Serial.println(msgInfo.errorCode);*/
	#endif
	jsonArraySize = getNArraySize(payloadptr);
	msgInfo.responseArraySize = jsonArraySize;
	#ifdef _DEBUG_
	/*Serial.print("msgInfo.responseArraySize : ");
	Serial.println(msgInfo.responseArraySize);*/
	#endif
		
	if(jsonArraySize > 0){
		for(nameIdx = 0; nameIdx < jsonArraySize;nameIdx++){
			getResourceUri(payloadptr,nameIdx,resourceUri[nameIdx]);
			nSize = strlen(resourceUri[nameIdx]);

			msgInfo.resourceUri[nameIdx] =(char*) malloc(sizeof(char)*nSize+1);
			strcpy(msgInfo.resourceUri[nameIdx],resourceUri[nameIdx]);
			
		}
		if(getStringValue(payloadptr,0,stringValue[0])!=0){
			for(svIdx = 0; svIdx<jsonArraySize; svIdx++){
				getStringValue(payloadptr,svIdx,stringValue[svIdx]);
				svSize = strlen(stringValue[svIdx]);

				msgInfo.stringValue[svIdx] = (char*)malloc(sizeof(char)*svSize+1);
				strcpy(msgInfo.stringValue[svIdx],stringValue[svIdx]);
			}
		}
		if(getTime(payloadptr,0,receiveTime[0])!=0){
			for(tiIdx = 0; tiIdx<jsonArraySize; tiIdx++){
				getTime(payloadptr,tiIdx,receiveTime[tiIdx]);
				tiSize = strlen(receiveTime[tiIdx]);
				
				msgInfo.time[tiIdx] = (char*)malloc(sizeof(char)*tiSize+1);
				strcpy(msgInfo.time[tiIdx],receiveTime[tiIdx]);
			}
		}
	}
	msgType = getMessageType(topicName,payloadptr);
	if(msgType == MESSAGE_TYPE_DELETE_SYNC)
	{
		damda_Rsp_RemoteDelDevice(msgInfo.sid,"200");
		damda_Req_DeleteDevice();
	}
	else if(msgType == MESSAGE_TYPE_DELETE_ASYNC)
	{
		damda_Req_DeleteDevice();
	}
	if(msgType == MESSAGE_TYPE_FIRMWARE_DOWNLOAD)
	{
		getTopicOprId(topicName,operationId);
		if(strcmp(operationId,"write") == 0)
		{
			damda_Rsp_RemoteControl_noEntity(MESSAGE_TYPE_WRITE,sid,"200");
		}
		getStringValueByResourceUri(payloadptr,"/5/0/1",firmwareDownloadUrl);
		//Serial.println(firmwareDownloadUrl);
		urllen = strlen(firmwareDownloadUrl);
		msgInfo.firmwareDownloadUrl = (char*)malloc(urllen+1);
		strcpy(msgInfo.firmwareDownloadUrl,firmwareDownloadUrl);
		
		getStringValueByResourceUri(payloadptr,"/3/0/3",firmwareVersion);
		//Serial.println(firmwareVersion);
		firmwareVersionlen = strlen(firmwareVersion);
		msgInfo.firmwareVersion = (char*)malloc(firmwareVersionlen+1);
		strcpy(msgInfo.firmwareVersion,firmwareVersion);
	}
	msgInfo.responseType = msgType;
	if(messageArrivedCallbackStatus == 1)
	{
		callbackFunc(&msgInfo);
	}
	for(freeIndex=0;freeIndex<jsonArraySize;freeIndex++)
	{
		free(msgInfo.resourceUri[freeIndex]);
		if(getStringValue(payloadptr,0,stringValue[0])!=0)
		{
			free(msgInfo.stringValue[freeIndex]);
		}
		if(getTime(payloadptr,0,receiveTime[0])!=0)
		{
			free(msgInfo.time[freeIndex]);
		}
	}
	
	if(msgType == MESSAGE_TYPE_FIRMWARE_DOWNLOAD)
	{
		free(msgInfo.firmwareDownloadUrl);
		free(msgInfo.firmwareVersion);
	}
}


void damda_messageReceived_callback_loop()
{
	client.loop();
}


void damda_MQTT_Connect(char* mqttUser,char* mqttPassword,char* mqttAddress,int mqttPort,char* device_type)
{
	int loop_count = 0;
	strcpy(damda_deviceType,device_type);
	char subscribeTopic[100];

	/************** Try to connect MQTT **************/
	client.setOptions(120, true, 1000);
	client.begin(mqttAddress,mqttPort,net);									//Mqtt client 시작
	client.onMessageAdvanced(messageReceived);                            	//CallBack Start  string langth 있음

	Serial.println(" ");
	Serial.print("at damda_MQTT_Connect -> MQTT_connecting...");
	client.connect(damda_deviceType, mqttUser, mqttPassword);				//connect Mqtt client
	Serial.println(" ");
	sprintf(subscribeTopic,"%s/+/iot-server/%s/#",damda_deviceType,mqttUser);
	client.subscribe(subscribeTopic);
	Serial.print("at damda_MQTT_Connect -> MQTT Connected success : ");
	Serial.println(subscribeTopic);
}


void damda_SetConnectInfo(char* server_addr,char* server_port,char* ap_ssid, char* ap_password,char* auth_key)
{
	damda_connInfo.serverAddr = server_addr;
	damda_connInfo.serverPort = server_port;
	damda_connInfo.apSsid = ap_ssid;
	damda_connInfo.apPassword = ap_password;
	damda_connInfo.authKey = auth_key;
}


void damda_setMQTTMsgReceiveCallback(MSG_ARRIVED_CALLBACK_FUNCTION func)
{
	callbackFunc = func;
	messageArrivedCallbackStatus = 1;
}


bool damda_IsMQTTConnected()
{
	return client.connected();
}


void damda_DisConnectMQTT()
{
	client.disconnect();
}


void damda_Secure()
{
	#ifdef _ESP8266_
	fetchCertAuthority();
	#else
	net.setCACert(ca_pem);
	#endif
}
