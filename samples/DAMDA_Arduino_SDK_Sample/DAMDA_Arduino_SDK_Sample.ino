#include "damdaCoreSDK.h"
#include <EEPROM.h>
#include <SoftwareSerial.h>
#include <LittleFS.h>
#include <ESP8266WebServer.h>
#include <ESP8266httpUpdate.h>
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>
#include <ESP8266WiFi.h>
#include <time.h>    					// libraries espressif para Arduino

#include <WiFiClient.h>


/* ESP8266 dev-kit's pinout
              +---------------+                             +---------------+
GPIO | DEVKIT |      DOIT     | DEVKIT | GPIO                This device sdk (select board as "Generic ESP8266 Module" at Aduino IDE)
     |        |       V4      |        |                     (Module : DOIT, ESP-F DEVKIT V4)
-----|--------+---------------+---------------------------------------------
     |    A0  | AD0        D0 |  D0    |  16         ---->     BUTTON_LED1
     |   RSV  | RSV       ~D1 |  D1    |   5         ---->     BUTTON_PIN1 : interrupt, used for device reset(deregister and eeprom clear)
     |   RSV  | RSV       ~D2 |  D2    |   4
 10  |   SD3  | SD3       ~D3 |  D3    |   0         ---->     BUTTON_PIN 
  9  |   SD2  | SD2       ~D4 |  D4    |   2
     |   SD1  | SD1       3V3 |  3V3   |
     |   CMD  | CMD       GND |  GND   |
     |   SD0  | SD0       ~D5 |  D5    |  14         ---->     BUTTON_LED2
     |   CLK  | CLK       ~D6 |  D6    |  12         ---->     BUTTON_LED3
     |   GND  | GND       ~D7 |  D7    |  13
     |   3V3  | 3V3       ~D8 |  D8    |  15
     |    EN  | EN        RX< |  D9    |   3
     |   RST  | RST       TX> |  D10   |   1
     |   GND  | GND       GND |  GND   |
     |   Vin  | Vin       3V3 |  3V3   |
              +---------------+

Device SDK revision change :
- 2022/04/10 : Board Manager ver 3.0.2 
  ** Board Manager ver 3.0.2 uses lots of memery(heap), need to decrease code as much as possible. 

*/

time_t now;
time_t prevEpoch;
struct tm * timeinfo;

//#define 	DEVICE_SWITCH
#define		DEVICE_PLUG
#define		ESP8266
#define 	_ARDUINO_

#ifdef DEVICE_SWITCH
	const int BUTTON_PIN = 0; 
	const int BUTTON_PIN1 = 5;
	const int BUTTON_LED1 = 16; 
	const int BUTTON_LED2 = 14; 
	const int BUTTON_LED3 = 12; 
	const int BUTTON_LED4 = 13; 
	const int DIMMING_LED = 15;
	
	const char *ssid      =   "ARDUINO_ESP8266_5";
	const char *password  =   "12345678";

	#define DEVICE_MANUFACTURER  	"WINIA"
	#define DEVICE_MODEL  			"WN_SWITCH"
	#define DEVICE_SERIAL  			"21002211"
	#define DEVICE_TYPE  			"SWITCH"
	
	String DEVICE_MANUFACTURER_BUFFER	= DEVICE_MANUFACTURER;
	String DEVICE_MODEL_BUFFER 		    = DEVICE_MODEL;
	String DEVICE_SERIAL_BUFFER 		= DEVICE_SERIAL;
	String DEVICE_TYPE_BUFFER 			= DEVICE_TYPE;
	String DEVICE_SERVICE_ID 			= "SWITCH";
	
	#define IPSET_STATIC  { 192, 168, 2, 80 }
	byte ip_static_bu[] = IPSET_STATIC;

	char* Power_state       =   "OFF";
	char* Power_Led1_state  = 	"OFF";
	char* Power_Led2_state  = 	"OFF";
	char* Power_Led3_state  = 	"OFF";
	char* Dimming_state     =	"16";
#endif

#ifdef DEVICE_PLUG
	const int BUTTON_PIN = 0; 
	const int BUTTON_PIN1 = 5;
	const int BUTTON_LED1 = 16; 
	const int BUTTON_LED2 = 14; 
	const int BUTTON_LED3 = 12; 
	const int BUTTON_LED4 = 13; 

	SoftwareSerial T_Serial(13,15); //RX, TX  
             
	const char *ssid =          "DAMDA_PLUG_0000";
	const char *password =      "12345678";
	
	#define DEVICE_MANUFACTURER "WINIA"
	#define DEVICE_MODEL        "WN_PLUG"
	#define DEVICE_SERIAL		"20220325"
	#define DEVICE_TYPE			"PLUG"  //"Arduino"
	
/*
	#define DEVICE_MANUFACTURER	"DAEWOO"
	#define DEVICE_MODEL		"DW_RFR"
	#define DEVICE_SERIAL		"90000000091"
	#define DEVICE_TYPE			"REFRIGERATOR"  //"Arduino"
*/

/*
	#define DEVICE_MANUFACTURER	"EASYSAVER"
	#define DEVICE_MODEL		"IMPLUG"
	#define DEVICE_SERIAL		"12000000026"
	#define DEVICE_TYPE			"STAND"  //"Arduino"
*/
	String DEVICE_MANUFACTURER_BUFFER = DEVICE_MANUFACTURER;
	String DEVICE_MODEL_BUFFER = DEVICE_MODEL;
	String DEVICE_SERIAL_BUFFER = DEVICE_SERIAL;
	String DEVICE_TYPE_BUFFER = DEVICE_TYPE;
	
	String DEVICE_SERVICE_ID = "PLUG"; 
	
	#define IPSET_STATIC  { 192, 168, 2, 81 }
	byte ip_static_bu[] = IPSET_STATIC;

	char* Power_state =	"OFF";
	char* Standby_cutoff_setting_state = "false";
	char* Standby_cutoff_state = "false";
	char* Overheat_state = "false";
#endif

#define  EEPROM_SIZE 255

String content;
String st;
int statusCode;

int booting_wifi_mode = 0; 	// variable for booting and wifi mode
 							// 1(any): Boot with Wifi Soft AP mode
							// 2     : Boot with Wifi Station mode
							// 3     : Boot with Wifi Station mode and MQTT connect (device is registed to platform)

int sendmessage_period_minute = 100 * 60 * 1;	// 1 minute for test only, need to adjust variable more than 5 minute.
												// 100 * 60 * 5 == 5 minute.

#define SOFT_IPSET_STATIC  	{ 10, 10, 10, 10 }         // IP address for SOFT AP(server mode)
//#define IPSET_STATIC  	{ 192, 168, 2, 81 }
#define IPSET_GATEWAY 		{ 192, 168, 2, 1   }
#define IPSET_SUBNET  		{ 255, 255, 255, 0  }
#define IPSET_DNS     		{ 210, 94, 0, 73   }

byte ip_static[4] = {};                               
byte ip_static_AP[] = SOFT_IPSET_STATIC;
//byte ip_static_bu[] = IPSET_STATIC;
byte ip_gateway[] = IPSET_GATEWAY;
byte ip_subnet[] = IPSET_SUBNET;
byte ip_dns[] = IPSET_DNS;

int ucNotify_sync_flag = 1; //0;  
int ucNotify_async_flag = 1; //0;

char System_Timer[] = "1649200922000";				// Wed Apr 06 2022 08:22:02, temporary system time(unix time stamp in milliseconds), need to update it using NTP or RTC.
													// This device SDK sample uses NTP when the device is connected to wifi at booting to update the time. 
char System_Timer_Set = 0;
String System_Timer_Count = "";

String server_addr ="";
String server_port = "";
String wifi_ssid ="";
String wifi_password ="";
String auth_key ="";

File fsUploadFile;                  				// a File object to temporarily store the received file

String UpdateFileUrl = "";
String Firmware_file_name = "mini.bin";
String Firmware_version = "1.0";

String Topic_Device_id = DEVICE_MANUFACTURER_BUFFER+"-"+DEVICE_MODEL_BUFFER+"-"+DEVICE_SERIAL_BUFFER;

unsigned char message_print_flag = 1; 				// 1 : enable serial menu output, 0 : disable 

unsigned int Timer_10ms_ReceiveCallBack = 0;
unsigned int Timer_10ms_SendMessage = 0;

String WiFi_IP_ADDRESS = "";

char Button_Control_Buffer = 0;

#define MAX_TIME 85
#define MAX_DEVICE_INFO_LEN 20
#define MAX_VERSION_LEN 10
#define MAX_FIRMWARE_DOWNLOAD_URL_LEN 512

ESP8266WebServer server(18080);

DAMDA_connectionInfo damda_sample_connection_info;

#ifdef DEVICE_SWITCH
NotifyParameter DeviceNotifyParameter[5];
#endif
#ifdef DEVICE_PLUG
NotifyParameter DeviceNotifyParameter[4];
#endif


char firmwareDownloadURL[MAX_FIRMWARE_DOWNLOAD_URL_LEN];
char firmwareVersion[MAX_VERSION_LEN];

void handlebody();

void (* resetFunc)(void) = 0;       // System RESET function


char* string2char(String command)
{
    if(command.length()!=0)
    {
      char *p = const_cast<char*>(command.c_str());
      return p;
    }
}


void printLocalTime() 
{
  //String buf;
  if (time(&now) != prevEpoch) 
  {
    Serial.print("Current UTC Time : ");  // Current UTC time 
	Serial.println(time(&now)); 

    timeinfo = localtime(&now);
    int dd = timeinfo->tm_mday;
    int MM = timeinfo->tm_mon + 1;
    int yy = timeinfo->tm_year +1900;
    int ss = timeinfo->tm_sec;
    int mm = timeinfo->tm_min;
    int hh = timeinfo->tm_hour;
    int week = timeinfo->tm_wday;
   
	/* // If board manager version is under 2.7.x, use below code.  
    prevEpoch = time(&now);
    buf = time(&now);

	itoa(time(&now), System_Timer, 10); // use this function from Board manager 3.x.x 
	    
    System_Timer_Count = string2char(buf+"000");
    if(System_Timer_Count != NULL)
    {
      strcpy(System_Timer,string2char(System_Timer_Count));
    }
	*/

	itoa(time(&now), System_Timer, 10); // use this function from Board manager 3.x.x 
	strcat (System_Timer, "000");
	Serial.print("Current Uinx time stamp : ");  // Current unix time in milliseconds
	Serial.println(System_Timer); 

  }
}


void damda_LittleFS() 			// Start the SPIFFS and list all contents
{ 								
  	LittleFS.begin();      	    // Start the SPI Flash File System (SPIFFS)
  	Serial.println("LittleFS started. Contents:");
  	{
    	Dir dir = LittleFS.openDir("/");
    	while (dir.next())      // List the file system contents
    	{                      	
      		String fileName = dir.fileName();
      		size_t fileSize = dir.fileSize();
      		Serial.printf("\tFS File: %s, size: %s\r\n", fileName.c_str(), formatBytes(fileSize).c_str());
    	}
    	Serial.printf("\n");
  	}
}


void LittleFS_format()         // SPIFFS format
{
  	Serial.println("LittleFS format...START");
  
  	bool formatted = LittleFS.format();
  	if(formatted)
  	{
    	Serial.println("\n\nSuccess formatting");
  	}
  	else
  	{
    	Serial.println("\n\nError formatting");
  	}
  	Serial.println("LittleFS format...END");
}


void launchWeb(int webtype) 
{
  	Serial.println("");
  	Serial.print("at launchWeb - Local IP: ");
  	Serial.println(WiFi.localIP());
  	Serial.print("at launchWeb - SoftAP IP: ");
  	Serial.println(WiFi.softAPIP());
  	createWebServer(webtype);
  	
  	server.begin();
  	Serial.println("Server started"); 
}


void createWebServer(int webtype)
{
  content = "";
  if ( webtype == 1 ) 
  {
    server.on("/", []() 
    {
      IPAddress ip = WiFi.softAPIP();
      String ipStr = String(ip[0]) + '.' + String(ip[1]) + '.' + String(ip[2]) + '.' + String(ip[3]);
      content = "<!DOCTYPE HTML>\r\n<html>Hello from ESP8266 at ";
      content += ipStr;
      content += "<p>";
      content += st;

      content += "</p><form method='POST' action='setting'><label>server_addr: </label><input name='server_addr' length=32></p>";
      content += "<label>server_port: </label><input name='server_port' length=8></p>";
      content += "<label>wifi_ssid: </label><input name='wifi_ssid' length=32></p>";
      content += "<label>wifi_password: </label><input name='wifi_password' length=32></p>";
      content += "<label>auth_key: </label><input name='auth_key' length=64></p>";
      content += "<label>wifi_ip: </label><input name='wifi_ip' length=32><input type='submit'></form>";
      content += "</html>";
      server.send(200, "text/html", content);  
    });

    server.on("/api/v1/wifi/info/get",HTTP_GET,[](){								    // 모바일에서 서버 연결 요청  모바일 -> 디바이스
      	server.send(200, "application/json", "{\"manufacturer\":\""+DEVICE_MANUFACTURER_BUFFER+"\",\"model\":\""+DEVICE_MODEL_BUFFER+"\",\"serial\":\""+DEVICE_SERIAL_BUFFER+"\",\"support_type\":\""+DEVICE_TYPE_BUFFER+"\"}");
    });
        
    server.on("/api/v1/wifi/link/start", HTTP_POST, handlebody);						// 모바일에서 서버 연동 시작
    server.on("/api/download", HTTP_POST, damda_firmware_download);						// 서버에서 펌웨어 다운로드
    server.on("/api/firmwareupdate", HTTP_POST, damda_firmware_update);					// SPIFFS에서 펌웨어 업데이트
	server.on("/api/format", HTTP_POST,  LittleFS_format);								// LittleFS_format 포멧
  	server.on("/eeprom", HTTP_POST,  damda_Eeprom_format);								// EEPROM 포멧
	server.on("/api/v1/wifi/link/set", HTTP_POST, damda_WiFi_Reconnect);				// 모바일에서 Wifi 재설정 정보를 보내줌
	
    server.on("/upload", HTTP_GET, []() {												// if the client requests the upload page     //파일 다운로드
      if (!handleFileRead("/"+Firmware_file_name))										// send it if it exists
        server.send(404, "text/plain", "404: Not Found"); 							  	// otherwise, respond with a 404 (Not Found) error
    });

    server.on("/upload", HTTP_POST,                       							  	// if the client posts to the upload page     //파일 직접 Upload
      [](){ server.send(200); },                          							  	// Send status 200 (OK) to tell the client we are ready to receive
      handleFileUpload                                    							  	// Receive and save the file
    );
    
    server.onNotFound([]() {                              							  	// If the client requests any URI
      if (!handleFileRead(server.uri()))                  							  	// send it if it exists
        server.send(404, "text/plain", "404: Not Found"); 							  	// otherwise, respond with a 404 (Not Found) error
    });
    
    server.on("/setting", []() {
        server_addr = server.arg("server_addr");
        server_port = server.arg("server_port");
        wifi_ssid = server.arg("wifi_ssid");
        wifi_password = server.arg("wifi_password");
        auth_key = server.arg("auth_key");
        String WiFi_IP_ADDRESS = server.arg("wifi_ip");
        const char * c = WiFi_IP_ADDRESS.c_str();
        
        if (wifi_ssid.length() > 0 && wifi_password.length() > 0) 
        {
			Serial.println("clearing eeprom");
			for (int i = 0; i < EEPROM_SIZE; ++i) 
			{
				EEPROM.write(i, 0); 
			}
			EEPROM.commit();
			Serial.print("writing eeprom ssid:");
           
        	if (server_addr.length() > 0)
          	{
				for (int i = 0; i < server_addr.length(); ++i)
				{
					EEPROM.write(i, server_addr[i]);
					Serial.print("server_addr: ");
					Serial.println(server_addr[i]); 
				}  	        	
				for (int i = 0; i < server_port.length(); ++i)
				{
					EEPROM.write(32+i, server_port[i]);
					Serial.print("server_port: ");
					Serial.println(server_port[i]); 
				}
				for (int i = 0; i < wifi_ssid.length(); ++i)
				{
					EEPROM.write(40+i, wifi_ssid[i]);
					Serial.print("wifi_ssid: ");
					Serial.println(wifi_ssid[i]); 
				}
				for (int i = 0; i < wifi_password.length(); ++i)
				{
					EEPROM.write(72+i, wifi_password[i]);
					Serial.print("wifi_password: ");
					Serial.println(wifi_password[i]); 
				}
				for (int i = 0; i < auth_key.length(); ++i)
				{
					EEPROM.write(104+i, auth_key[i]);
					Serial.print("auth_key: ");
					Serial.println(auth_key[i]); 
				}
				for (int i = 0; i < WiFi_IP_ADDRESS.length(); ++i)
				{
					EEPROM.write(140+i, WiFi_IP_ADDRESS[i]);
					Serial.print("Write: ");
					Serial.println(WiFi_IP_ADDRESS[i]); 
				}
  	        EEPROM.write(EEPROM_SIZE, 3);
        }

        EEPROM.commit();
        
  		/************************ IP 주소값을 0.0.0.0   .단위로 잘라낸다 ************************/
        char ipChar[WiFi_IP_ADDRESS.length()+1];
        WiFi_IP_ADDRESS.toCharArray(ipChar,WiFi_IP_ADDRESS.length()+1);
        char *p = ipChar;
        char *str;     
        int index=0;
        while ((str = strtok_r(p, ".", &p)) != NULL)
        {
			// delimiter is the semicolon
			ip_static[index] = byte(atoi(str));
			index++;
        }
		 	  
  		/*********************************************************************/
        content = "{\"Success\":\"saved to eeprom... reset to boot into new wifi\"}";
        statusCode = 200;
        Serial.println("Send 200");
    } 
	else 
	{
		content = "{\"Error\":\"404 not found\"}";
		statusCode = 404;
		Serial.println("Send 404");
	}
	server.send(statusCode, "application/json", content);		// EEPROM 성공 실패 메세지 출력
	
	WiFi.mode(WIFI_AP_STA);
	WiFi.config(IPAddress(ip_static_bu), IPAddress(ip_gateway), IPAddress(ip_subnet), IPAddress(ip_dns));
	Serial.println(WiFi.localIP());
	WiFi.begin(wifi_ssid.c_str(), wifi_password.c_str());
	return;
    });
  }
}


void handlebody()           // 모바일 데이터 송신 및 수신
{
	server.send(200, "application/json", "{}"); 
	delay(1000);
	StaticJsonDocument<300> newBuffer;
	deserializeJson(newBuffer, server.arg("plain"));                              // buffer에 body 내용 json으로 변환
	JsonObject bodyContent = newBuffer.as<JsonObject>();                          // json 내용 jsonObject에 할당
	JsonVariant server_addrVariant = bodyContent.getMember("server_addr");        // server_addr 내용 가져옴
	server_addr = server_addrVariant.as<const char*>();                    		    // 내용 String으로 형변환
	JsonVariant server_portVariant = bodyContent.getMember("server_port");
	server_port = server_portVariant.as<const char*>();
	JsonVariant wifi_ssidVariant = bodyContent.getMember("ap_ssid");
	wifi_ssid = wifi_ssidVariant.as<const char*>();
	JsonVariant wifi_passwordVariant = bodyContent.getMember("ap_password");
	wifi_password = wifi_passwordVariant.as<const char*>();
	JsonVariant auth_keyVariant = bodyContent.getMember("auth_key");
	auth_key = auth_keyVariant.as<const char*>();

	damda_SetConnectInfo((char *)server_addr.c_str(),(char *)server_port.c_str(),(char *)wifi_ssid.c_str(),(char *)wifi_password.c_str(),(char *)auth_key.c_str());

	damda_sample_connection_info = getConnectionInfo();

	Serial.print("server_addr : ");
	Serial.println(damda_sample_connection_info.serverAddr);
	Serial.print("server_port : ");
	Serial.println(damda_sample_connection_info.serverPort);
	Serial.print("WiFi_ssid : ");						         
	Serial.println(damda_sample_connection_info.apSsid);
	Serial.print("WiFi_password : ");				            
	Serial.println(damda_sample_connection_info.apPassword);
	Serial.print("auth_key : ");
	Serial.println(damda_sample_connection_info.authKey);

	//******************* Save server & wifi information in the EEPROM *******************/
	if (wifi_ssid.length() > 0)
	{
		Serial.println("clearing eeprom_IPAddress");
		for (int i = 0; i <= EEPROM_SIZE; ++i) 
		{ 
			EEPROM.write(i, 0); 
		}
		for (int i = 0; i < server_addr.length(); ++i)
		{
			EEPROM.write(i, server_addr[i]);
			Serial.print("server_addr: ");
			Serial.println(server_addr[i]); 
		}
		for (int i = 0; i < server_port.length(); ++i)
		{
			EEPROM.write(32+i, server_port[i]);
			Serial.print("server_port: ");
			Serial.println(server_port[i]); 
		}
		for (int i = 0; i < wifi_ssid.length(); ++i)
		{
			EEPROM.write(40+i, wifi_ssid[i]);
			Serial.print("wifi_ssid: ");
			Serial.println(wifi_ssid[i]); 
		}
		for (int i = 0; i < wifi_password.length(); ++i)
		{
			EEPROM.write(72+i, wifi_password[i]);
			Serial.print("wifi_password: ");
			Serial.println(wifi_password[i]); 
		}
		for (int i = 0; i < auth_key.length(); ++i)
		{
			EEPROM.write(104+i, auth_key[i]);
			Serial.print("auth_key: ");
			Serial.println(auth_key[i]); 
		}
		for (int i = 0; i < WiFi_IP_ADDRESS.length(); ++i)
		{
			//EEPROM.write(136+i, WiFi_IP_ADDRESS[i]);
			EEPROM.write(140+i, WiFi_IP_ADDRESS[i]);
			Serial.print("WiFi_IP_ADDRESS: ");
			Serial.println(WiFi_IP_ADDRESS[i]);
		}
   		//EEPROM.write(EEPROM_SIZE, 2); 	// Device mode value is saved in end of eeprom address(255) // '2' means "STATION MODE START" 
		EEPROM.write(EEPROM_SIZE, 3); 		// Device mode value is saved in end of eeprom address(255) // '3' means "STATION MODE / START_MQTT CONNECT" 
 	}

	delay(100);
	EEPROM.commit();
	delay(100);
	ESP.restart();
	return;
}


String getContentType(String filename)
{
	if(filename.endsWith(".htm")) return "text/html";
	else if(filename.endsWith(".html")) return "text/html";
	else if(filename.endsWith(".css")) return "text/css";
	else if(filename.endsWith(".js")) return "application/javascript";
	else if(filename.endsWith(".png")) return "image/png";
	else if(filename.endsWith(".gif")) return "image/gif";
	else if(filename.endsWith(".jpg")) return "image/jpeg";
	else if(filename.endsWith(".ico")) return "image/x-icon";
	else if(filename.endsWith(".xml")) return "text/xml";
	else if(filename.endsWith(".pdf")) return "application/x-pdf";
	else if(filename.endsWith(".zip")) return "application/x-zip";
	else if(filename.endsWith(".gz")) return "application/x-gzip";
	else if(filename.endsWith(".bin")) return "application/octet-stream";
	return "text/plain";
}


bool handleFileRead(String path)                            // send the right file to the client (if it exists)
{
	timer1_disable();
	server.send(200,"OK");
	Serial.println("handleFileRead: " + path);
	if (path.endsWith("/")) path += Firmware_file_name;			// If a folder is requested, send the index file
	String contentType = getContentType(path);					// Get the MIME type
	String pathWithGz = path + ".gz";
	if (SPIFFS.exists(pathWithGz) || SPIFFS.exists(path))		// If the file exists, either as a compressed archive, or normal
	{
		if (SPIFFS.exists(pathWithGz))                          // If there's a compressed version available
		path += ".gz";                                          // Use the compressed verion
		File file = SPIFFS.open(path, "r");                     // Open the file
		size_t sent = server.streamFile(file, contentType);     // Send it to the client
		file.close();                                           // Close the file again
		Serial.println(String("\tSent file: ") + path);
		return true;
	}
	Serial.println(String("\tFile Not Found: ") + path);		// If the file doesn't exist, return false
	//client.loop();								            // MQTT Call back 모니터링(1,0,1);
	return false;
}


void handleFileUpload()                                    		// upload a new file to the SPIFFS
{
	timer1_disable();
	server.send(200,"UPDATE_START");
	HTTPUpload& upload = server.upload();
	if(upload.status == UPLOAD_FILE_START)
	{
		String filename = upload.filename;
		if(!filename.startsWith("/")) filename = "/"+filename;
		Serial.print("handleFileUpload Name: "); Serial.println(filename);
		fsUploadFile = SPIFFS.open(filename, "w");             // Open the file for writing in SPIFFS (create if it doesn't exist)
		filename = String();
	} 
	else if(upload.status == UPLOAD_FILE_WRITE)
	{
		if(fsUploadFile)
		fsUploadFile.write(upload.buf, upload.currentSize);    // Write the received bytes to the file
	} 
	else if(upload.status == UPLOAD_FILE_END)
	{
		if(fsUploadFile)                                       // If the file was successfully created
		{
			fsUploadFile.close();                                // Close the file again
			Serial.print("handleFileUpload Size: "); Serial.println(upload.totalSize);
			server.sendHeader("Location","/"+Firmware_file_name);      // Redirect the client to the success page
			server.send(303);
  		} 
		else
		{
		server.send(500, "text/plain", "500: couldn't create file");
		}
	}
	server.send(200,"OK");
	timer1_enable(1,0,1);
}


String formatBytes(size_t bytes) 					// convert sizes in bytes to KB and MB
{ 
	if (bytes < 1024)
	{
		return String(bytes) + "B";
	} 
	else if (bytes < (1024 * 1024)) 
	{
    	return String(bytes / 1024.0) + "KB";
	} 
	else if (bytes < (1024 * 1024 * 1024)) 
	{
		return String(bytes / 1024.0 / 1024.0) + "MB";
 	}
}


void message_callback(ResponseMessageInfo* ms)
{
	timer1_disable();
	
	int i = 0;
	int msgType = 0;
	const int arraySize = ms->responseArraySize;
	#ifdef DEVICE_SWITCH
	char* refrigeratorStringValue[5] = {Power_state,Power_Led1_state,Power_Led2_state,Power_Led3_state,Power_Led3_state};
	#endif
	#ifdef DEVICE_PLUG
	char* refrigeratorStringValue[4] = {Power_state,Standby_cutoff_setting_state,Standby_cutoff_state,Overheat_state};
	#endif
	
	#ifdef DEVICE_SWITCH
		if(ms->responseType == MESSAGE_TYPE_READ || ms->responseType == MESSAGE_TYPE_WRITE || ms->responseType == MESSAGE_TYPE_EXECUTE || ms->responseType == MESSAGE_TYPE_FIRMWARE_DOWNLOAD)
		{
			if(strcmp(ms->resourceUri[0],"/3306/0/5853") == 0)
      		{
				if(strcmp(ms->stringValue[0],"ON") == 0)Power_state = "ON";
				else if(strcmp(ms->stringValue[0],"OFF") == 0)Power_state = "OFF";
				else Power_state = "OFF";
				Serial.println("----------- Power_state ---------");
				Serial.println(Power_state);
			}
			else if(strcmp(ms->resourceUri[0],"/3312/0/5850") == 0)
			{
				if(strcmp(ms->stringValue[0],"ON") == 0)Power_Led1_state = "ON";
				else Power_Led1_state = "OFF";
				Serial.println("----------- Power_Led1_state ---------");
				Serial.println(Power_Led1_state);
			}
			else if(strcmp(ms->resourceUri[0],"/3312/0/5851") == 0)
			{
				if(strcmp(ms->stringValue[0],"ON") == 0)Power_Led2_state = "ON";
				else Power_Led2_state = "OFF";
				Serial.println("----------- Power_Led2_state ---------");
				Serial.println(Power_Led2_state);
			}
			else if(strcmp(ms->resourceUri[0],"/3312/0/5852") == 0)
			{
				if(strcmp(ms->stringValue[0],"ON") == 0)Power_Led3_state = "ON";
				else Power_Led3_state = "OFF";
				Serial.println("---------- Power_Led3_state ----------");
				Serial.println(Power_Led3_state);
			}
			else if(strcmp(ms->resourceUri[0],"/3343/0/5548") == 0)
			{
				if(strcmp(ms->stringValue[0],"16") == 0)Dimming_state = "16";
				else if(strcmp(ms->stringValue[0],"42") == 0)Dimming_state = "42";
				else if(strcmp(ms->stringValue[0],"83") == 0)Dimming_state = "83";
				else Dimming_state = "0";
				Serial.println("---------- Dimming_state ----------");
				Serial.println(Dimming_state);
			}						
			if(ms->responseType == MESSAGE_TYPE_READ)
			{
				damda_Rsp_RemoteControl(MESSAGE_TYPE_READ,ms->sid,"200",ms->resourceUri,refrigeratorStringValue,arraySize);	
			} 
			else if(ms->responseType == MESSAGE_TYPE_WRITE)
			{	
				damda_Rsp_RemoteControl(MESSAGE_TYPE_WRITE,ms->sid,"200",ms->resourceUri,ms->stringValue,arraySize);
			} 
			else if(ms->responseType == MESSAGE_TYPE_EXECUTE)
			{	
				damda_Rsp_RemoteControl(MESSAGE_TYPE_EXECUTE,ms->sid,"200",ms->resourceUri,ms->stringValue,arraySize);
			} 
		}
		if(ms->responseType == MESSAGE_TYPE_DELETE_SYNC || ms->responseType == MESSAGE_TYPE_DELETE_ASYNC)
		{
			Serial.println("Soft AP ON");
			Serial.println("Wifi Disconnect");
			Serial.println("EEPROM format");
			//timer1_disable();
			damda_Eeprom_format();
			delay(10);
			EEPROM.write(EEPROM_SIZE, 1);		// Soft AP ON
			EEPROM.commit();
			delay(10);
			Serial.println("Soft AP MODE START");
			delay(10);
			ESP.restart();
		}
		if(ms->responseType == MESSAGE_TYPE_FIRMWARE_DOWNLOAD)
		{
			strcpy(firmwareDownloadURL,ms->firmwareDownloadUrl);
			strcpy(firmwareVersion,ms->firmwareVersion);
			UpdateFileUrl = firmwareDownloadURL;
		}
	#endif

	#ifdef DEVICE_PLUG
		if(ms->responseType == MESSAGE_TYPE_READ || ms->responseType == MESSAGE_TYPE_WRITE || ms->responseType == MESSAGE_TYPE_EXECUTE || ms->responseType == MESSAGE_TYPE_FIRMWARE_DOWNLOAD)
		{				
			if(strcmp(ms->resourceUri[0],"/3312/0/5850") == 0)
			{			
				if(strcmp(ms->stringValue[0],"ON") == 0)Power_state = "ON";
				else if(strcmp(ms->stringValue[0],"OFF") == 0)Power_state = "OFF";
				else Power_state = "OFF";
				Serial.print("Power_state :");
				Serial.println(Power_state);
			}
			else if(strcmp(ms->resourceUri[0],"/3306/0/5850") == 0)
			{
				if(strcmp(ms->stringValue[0],"true") == 0)Standby_cutoff_setting_state = "true";
				else Standby_cutoff_setting_state = "false";
				Serial.print("Standby_cutoff_setting_state :");
				Serial.println(Standby_cutoff_setting_state);
			}
			else if(strcmp(ms->resourceUri[0],"/3342/0/5500") == 0)
			{
				if(strcmp(ms->stringValue[0],"true") == 0)Standby_cutoff_state = "true";
				else Standby_cutoff_state = "false";
				Serial.print("Standby_cutoff_state :");
				Serial.println(Standby_cutoff_state);
			}
			else if(strcmp(ms->resourceUri[0],"/3342/0/5501") == 0)
			{
				if(strcmp(ms->stringValue[0],"true") == 0)Overheat_state = "true";
				else Overheat_state = "false";
				Serial.print("Overheat_state :");
				Serial.println(Overheat_state);
			}
			
			if(ms->responseType == MESSAGE_TYPE_READ)
			{
				damda_Rsp_RemoteControl(MESSAGE_TYPE_READ,ms->sid,"200",ms->resourceUri,refrigeratorStringValue,arraySize);	
			} 
			else if(ms->responseType == MESSAGE_TYPE_WRITE)
			{	
				damda_Rsp_RemoteControl(MESSAGE_TYPE_WRITE,ms->sid,"200",ms->resourceUri,ms->stringValue,arraySize);
			} 
			else if(ms->responseType == MESSAGE_TYPE_EXECUTE)
			{	
				damda_Rsp_RemoteControl(MESSAGE_TYPE_EXECUTE,ms->sid,"200",ms->resourceUri,ms->stringValue,arraySize);
			} 
			else if(ms->responseType == MESSAGE_TYPE_FIRMWARE_DOWNLOAD)
			{
				strcpy(firmwareDownloadURL,ms->firmwareDownloadUrl);
				strcpy(firmwareVersion,ms->firmwareVersion);	
			}
		}
		if(ms->responseType == MESSAGE_TYPE_DELETE_SYNC || ms->responseType == MESSAGE_TYPE_DELETE_ASYNC)
		{
			Serial.println("Soft AP ON");
			Serial.println("Wifi Disconnect");
			Serial.println("EEPROM format");
			
			damda_Eeprom_format();
			delay(10);
			EEPROM.write(EEPROM_SIZE, 1);		// Soft AP ON
			
			EEPROM.commit();
			delay(10);
			
			Serial.println("Soft AP MODE START");
			delay(10);
			ESP.restart();
		}
		if(ms->responseType == MESSAGE_TYPE_FIRMWARE_DOWNLOAD)
		{
			strcpy(firmwareDownloadURL,ms->firmwareDownloadUrl);
			UpdateFileUrl = firmwareDownloadURL;
		}
	#endif
		
		message_print_flag = 1;
		timer1_enable(1,0,1);
}


void LED_Contol()
{
	#ifdef DEVICE_SWITCH
	if(Power_state == "ON")digitalWrite(BUTTON_LED1, HIGH);	
	else digitalWrite(BUTTON_LED1, LOW);	

	if(Power_Led1_state == "ON")digitalWrite(BUTTON_LED2, HIGH);	
	else digitalWrite(BUTTON_LED2, LOW);	
		
	if(Power_Led2_state == "ON")digitalWrite(BUTTON_LED3, HIGH);	
	else digitalWrite(BUTTON_LED3, LOW);	
		
	if(Power_Led3_state == "ON")digitalWrite(BUTTON_LED4, HIGH);	
	else digitalWrite(BUTTON_LED4, LOW);	
		
	if(Dimming_state == "16")digitalWrite(DIMMING_LED, LOW);	
	else digitalWrite(DIMMING_LED, HIGH);	
	#endif
	
	#ifdef DEVICE_PLUG
	/*
	if(Power_state == "ON")T_Serial.write('1');	
	else T_Serial.write('2');

	if(Standby_cutoff_setting_state == "true")T_Serial.write('3');
	else T_Serial.write('4');	
		
	if(Standby_cutoff_state == "true")T_Serial.write('5');
	else T_Serial.write('6');
		
	if(Overheat_state == "true")T_Serial.write('7');
	else T_Serial.write('8');
	*/
	if(Power_state == "ON")digitalWrite(BUTTON_LED1, HIGH);	
	else digitalWrite(BUTTON_LED1, LOW);	
	
	if(Standby_cutoff_setting_state == "true")digitalWrite(BUTTON_LED2, HIGH);	
	else digitalWrite(BUTTON_LED2, LOW);	
		
	if(Standby_cutoff_state == "true")digitalWrite(BUTTON_LED3, HIGH);	
	else digitalWrite(BUTTON_LED3, LOW);	
		
	if(Overheat_state == "true")digitalWrite(BUTTON_LED4, HIGH);	
	else digitalWrite(BUTTON_LED4, LOW);	
	#endif
}


/* Send data with device resource */
void Notify_sync()
{
  	const char* sid = "1";
	if(ucNotify_sync_flag == 1)sid = "1";
	else if(ucNotify_async_flag == 1)sid = "2";

	printLocalTime(); 											// Copy time info to ti(unix timestamp)
		
	#ifdef DEVICE_PLUG
	DeviceNotifyParameter[0].resourceUri = "/3312/0/5850";
  	DeviceNotifyParameter[1].resourceUri = "/3306/0/5850";
  	DeviceNotifyParameter[2].resourceUri = "/3342/0/5500";
  	DeviceNotifyParameter[3].resourceUri = "/3342/0/5501";

	DeviceNotifyParameter[0].stringValue = Power_state;
	DeviceNotifyParameter[1].stringValue = Standby_cutoff_setting_state;
	DeviceNotifyParameter[2].stringValue = Standby_cutoff_state;
	DeviceNotifyParameter[3].stringValue = Overheat_state;
		
	DeviceNotifyParameter[0].time = System_Timer;
	DeviceNotifyParameter[1].time = System_Timer;
	DeviceNotifyParameter[2].time = System_Timer;
	DeviceNotifyParameter[3].time = System_Timer;
		
	damda_Notify_DeviceStatus(sid,DeviceNotifyParameter,4);
	ucNotify_async_flag = 0;
	Serial.println("at Notify_sync : damda_Notify_DeviceStatus ");  //jake
	#endif

	#ifdef DEVICE_SWITCH
	DeviceNotifyParameter[0].resourceUri = "/3306/0/5853";
  	DeviceNotifyParameter[1].resourceUri = "/3312/0/5850";
  	DeviceNotifyParameter[2].resourceUri = "/3312/0/5851";
  	DeviceNotifyParameter[3].resourceUri = "/3312/0/5852";
  	DeviceNotifyParameter[4].resourceUri = "/3343/0/5548";
	     
  	DeviceNotifyParameter[0].stringValue = Power_state;
	DeviceNotifyParameter[1].stringValue = Power_Led1_state;
	DeviceNotifyParameter[2].stringValue = Power_Led2_state;
	DeviceNotifyParameter[3].stringValue = Power_Led3_state;
	DeviceNotifyParameter[4].stringValue = Dimming_state;
		
	DeviceNotifyParameter[0].time = System_Timer;
	DeviceNotifyParameter[1].time = System_Timer;
	DeviceNotifyParameter[2].time = System_Timer;
	DeviceNotifyParameter[3].time = System_Timer;
	DeviceNotifyParameter[4].time = System_Timer;
	damda_Notify_DeviceStatus(sid,DeviceNotifyParameter,5);
	ucNotify_async_flag = 0;
 	#endif
	//Serial.printf("at Notify_sync : Free Heap size = %d \r\n", ESP.getFreeHeap());
}


/* Sampel control function, neet to modify custom behavior */
void control()
{
	char Button_Control = digitalRead (BUTTON_PIN);
	if(Button_Control == LOW)
	{
		if(Button_Control_Buffer == 0)
		{
			Button_Control_Buffer = 1;
      
			#ifdef DEVICE_SWITCH				
			if(strcmp(Power_state,"OFF") == 0)
			{
				Power_state = "ON";
			}
			else
			{
				Power_state = "OFF";
			}
			
			char* sid = "2";
      		DeviceNotifyParameter[0].resourceUri = "/3306/0/5853";
     		DeviceNotifyParameter[1].resourceUri = "/3312/0/5850";
      		DeviceNotifyParameter[2].resourceUri = "/3312/0/5851";
      		DeviceNotifyParameter[3].resourceUri = "/3312/0/5852";
      		DeviceNotifyParameter[4].resourceUri = "/3343/0/5548";
			
			DeviceNotifyParameter[0].stringValue = Power_state;
			DeviceNotifyParameter[1].stringValue = Power_Led1_state;
			DeviceNotifyParameter[2].stringValue = Power_Led2_state;
			DeviceNotifyParameter[3].stringValue = Power_Led3_state;
			DeviceNotifyParameter[4].stringValue = Dimming_state;
			
			DeviceNotifyParameter[0].time = System_Timer;
			DeviceNotifyParameter[1].time = System_Timer;
			DeviceNotifyParameter[2].time = System_Timer;
			DeviceNotifyParameter[3].time = System_Timer;
			DeviceNotifyParameter[4].time = System_Timer;
	        
      		damda_Notify_DeviceStatus(sid,DeviceNotifyParameter,5);
      		#endif

	  		#ifdef DEVICE_PLUG
			if(strcmp(Standby_cutoff_state,"true") == 0)
			{
				Standby_cutoff_state = "false";
				Overheat_state = "false";
			}
			else
			{
				Standby_cutoff_state = "true";
				Overheat_state = "true";
			}
			char* sid = "2";
			DeviceNotifyParameter[0].resourceUri = "/3312/0/5850";
      		DeviceNotifyParameter[1].resourceUri = "/3306/0/5850";
      		DeviceNotifyParameter[2].resourceUri = "/3342/0/5500";
      		DeviceNotifyParameter[3].resourceUri = "/3342/0/5501";
			
			DeviceNotifyParameter[0].stringValue = Power_state;
			DeviceNotifyParameter[1].stringValue = Standby_cutoff_setting_state;
			DeviceNotifyParameter[2].stringValue = Standby_cutoff_state;
			DeviceNotifyParameter[3].stringValue = Overheat_state;

			DeviceNotifyParameter[0].time = System_Timer;
			DeviceNotifyParameter[1].time = System_Timer;
			DeviceNotifyParameter[2].time = System_Timer;
			DeviceNotifyParameter[3].time = System_Timer;
				
	    	damda_Notify_DeviceStatus(sid,DeviceNotifyParameter,4);
			Serial.println("at control : damda_Notify_DeviceStatus "); //jake
     		#endif
		}
	}
	else
	{
		Button_Control_Buffer = 0;
	}
}


/* Command menu output throught serial port */
void Test_Command()
{
	if(message_print_flag == 1)
	{
		Serial.println(" ");
		Serial.println("1 : Soft AP ON");
		Serial.println("2 : Soft AP OFF");
		Serial.println("3 : Mqtt connect");
		Serial.println("4 : Register Device");
		Serial.println("5 : Update Device Information");
		Serial.println("6 : Timesync ON");
		Serial.println("7 : Report periodic information(notify)");
		Serial.println("8 : Report periodic information(notify) STOP");
		Serial.println("9 : Report event notification(notify), trigger virtual data");
		Serial.println("a : Unregister Device");
		Serial.println("b : Firmware Download init");
		Serial.println("c : Firmware Download");
		Serial.println("d : Firmware info update");
		Serial.println("e : Firmware update");
		Serial.println("f : EEPROM format");
		Serial.println("g : Custom menu");
		Serial.print("-- : ");
		Serial.println(DAMDA_SDK_VERSION);
		Serial.println("");
		message_print_flag = 0;
	}
}


/* Get command for the menu throught serial port */
void Test_Sample()
{
	NotifyParameter refrigeratorNotifyParam[1];
	NotifyParameter firmwareNotifyParam[2];
	refrigeratorNotifyParam[0].resourceUri = "/3342/0/5500";
	firmwareNotifyParam[0].resourceUri = "/5/0/3";
	firmwareNotifyParam[1].resourceUri = "/5/0/5";
	
	int i=0;
	char* ti;
	
	if(Serial.available())
	{
		//int ch = Serial.parseInt();
		char ch = Serial.read();
		switch(ch)
		{
			case '1' :							
			{
				delay(100);					
		  		timer1_disable();
		  		Serial.println("Soft AP ON");
		  		WiFi.disconnect(true);
		  		Serial.println("Wifi Disconnect");
		  		delay(100);
				Serial.println("***** Deregistering device is needed from Mobile App *****");
				Serial.println("Clear eeprom");
				for (int i = 0; i <= EEPROM_SIZE; ++i) 
				{ 
					EEPROM.write(i, 0); 
				}
		  		EEPROM.write(EEPROM_SIZE, 1);		// Write Soft AP ON flag in the eeprom 
		  		EEPROM.commit();
		  		delay(100);
				ESP.restart(); 						// Reboot device with Soft AP mode 
				break;
		  	} 
		  	case '2' : 
		  	{
				Serial.println("Soft AP OFF");
			  	timer1_disable();
			  	WiFi.disconnect(true);
			  	Serial.println("Wifi Disconnect");
			 	delay(1000);
		    	EEPROM.write(EEPROM_SIZE, 2);		// Soft AP off and STA ON
			  	EEPROM.commit();
			  	delay(100);
				damda_Read_EEPROM();
  				damda_WiFi_Connect();
  				Serial.println("WiFi STATION MODE START");
  				delay(100);
  				timer1_enable(1,0,1);
  				message_print_flag = 1;
		  		break;
  			}
		  	case '3' :
		  	{
				timer1_disable();
			  	Serial.println("Wifi Station mode and Mqtt connecting");
			  	delay(100);
			  	timer1_disable();
	      		EEPROM.write(EEPROM_SIZE, 3);		// STA ON and Mqtt connect
			  	EEPROM.commit();
			  	delay(100);
			  	ESP.restart();
			 	timer1_enable(1,0,1);
			  	break;
		  	}
			case '4' : 
			{
        		Serial.println("Register Device");
			  	damda_Req_RegisterDevice(); 
	    		break;
		  	}
		  	case '5' :
		  	{
				Serial.println("Update Device Information");
				damda_Req_UpdateDeviceInfo();
        		break;
		  	}
      		case '6' :     
			{
				Serial.println("Timesync ON");
        		damda_Req_SeverTimeInfo();
        		break;
		  	}
     		case '7' :   
			{
				Serial.println("Report periodic information(notify)");
    			ucNotify_sync_flag = 1;
			  	ucNotify_async_flag = 1;
			  	break;
		  	}
      		case '8' :          
      		{
				Serial.println("Report periodic information(notify) STOP");
       			ucNotify_sync_flag = 0;
				break;
      		}
      		case '9' :                  // Enable 비주기 보고 with virtual data for test 
      		{
				Serial.println("Report event notification(notify), trigger virtual data");
	      		ucNotify_async_flag = 1;
				// below, just for test to see data trigger
				if (Standby_cutoff_state == "false" && Overheat_state == "false") 
				{
					Standby_cutoff_state = "true";
					Overheat_state = "true";
					Notify_sync();
					break;
				}
				if (Standby_cutoff_state == "true" && Overheat_state == "true") 
				{
					Standby_cutoff_state = "false";
					Overheat_state = "false";
					Notify_sync();
					break;
				}
				// end for test
	      		break;
  			}
  			case 'a' : 
  			{
				Serial.println("Unregister Device");
			  	damda_Req_DeregisterDevice();
        		break;
		  	}
		  	case 'b' :
			{
				Serial.println("Firmware Download Init");
				damda_Req_UpdateDeviceInfo();
		
				ti = System_Timer;
			  	firmwareNotifyParam[0].stringValue = "0";
			  	firmwareNotifyParam[0].time = ti;
			  	firmwareNotifyParam[1].stringValue = "0";
			  	firmwareNotifyParam[1].time = ti;
			  	damda_Notify_DeviceStatus("2",firmwareNotifyParam,2);		// /5/0/3 /5/0/5 and string Value 0,0 send (download init)
			  	firmwareNotifyParam[0].stringValue="1";
			  	firmwareNotifyParam[0].time = ti;
			  	damda_Notify_DeviceStatus("2",firmwareNotifyParam,1);		// /5/0/3 and string Value 1 send (downloading)
			  	break;
		  	}
		 	case 'c' :
		  	{
        		damda_firmware_download();
	      		ti = System_Timer;
	      		firmwareNotifyParam[0].stringValue="2";
			  	firmwareNotifyParam[0].time = ti;
				break;
      		}
		  	case 'd' :
		  	{
				Serial.println("Firmware info update");
				ti = System_Timer;
				firmwareNotifyParam[0].stringValue="3";
				firmwareNotifyParam[0].time = ti;
				damda_Notify_DeviceStatus("2",firmwareNotifyParam,1);		// /5/0/3 and string Value 3 send (firmware installing)
				firmwareNotifyParam[0].stringValue="0";
				firmwareNotifyParam[0].time = ti;
				firmwareNotifyParam[1].stringValue="1";
				firmwareNotifyParam[1].time = ti;
				damda_Notify_DeviceStatus("2",firmwareNotifyParam,2); 		// /5/0/3 /5/0/5 and string Value 0,1 send (firmware has been installed)
				firmwareNotifyParam[1].stringValue="0";
				damda_setupDeviceInfo(DEVICE_MANUFACTURER,DEVICE_MODEL,DEVICE_SERIAL,firmwareVersion,DEVICE_TYPE);
				damda_Req_UpdateDeviceInfo();
			 	break;
		  	}
		  	case 'e' :
		  	{
        		Serial.println("Firmware update");
				damda_firmware_update();
		    	break;
		  	}
	    	case 'f' :
	    	{
        		Serial.println("### EEPROM format ###");
        		damda_Eeprom_format();
	      		delay(10);
				EEPROM.write(EEPROM_SIZE, 1);   // Soft AP ON
				EEPROM.commit();
				delay(10);
				Serial.println("### Soft AP MODE START ###");
				delay(10);
				ESP.restart();
				break;
	    	}
			case 'g' :
	    	{
        		Serial.println("User custom menu, put your code here ###");
        		delay(10);
				break;
	    	}
			default :
			break;
		}
	}
}


void damda_firmware_download()               			// WEB SERVER로 부터 파일을 다운로드 하여, SPIFFS에 저장
{
	WiFiClient wifiClient;

	timer1_disable();
	Serial.println("Timer_Disable OK");
	
	server.send(200,"OK");
  	HTTPClient http;
  
	String response = "";
	String url = String(UpdateFileUrl) + response.substring(4);
  	String file_name = response.substring(response.lastIndexOf('/'));
    
  	Serial.println(url);
  	File f = SPIFFS.open("/"+Firmware_file_name, "w");
	if (f)
	{
		//http.begin(url); // obsolete API
		http.begin(wifiClient, url);
		int httpCode = http.GET();
		if (httpCode > 0) 
		{
			if (httpCode == HTTP_CODE_OK) 
			{
				http.writeToStream(&f);
			}
		}
		else 
		{
			Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
		}
		f.close();
	}
	http.end();
	server.send(200,"Complate");
	delay(100);
	Serial.println("Firmware Download complate");
	damda_LittleFS();
	timer1_enable(1,0,1);
  	message_print_flag = 1;
}


void damda_firmware_update()           			// SPIFFS에 들어있는 firmware 파일을 직접 Update 한다.
{
  	timer1_disable();
	server.send(200,"OK");
	Dir dir = SPIFFS.openDir("/");
	File file = SPIFFS.open("/"+Firmware_file_name, "r");
  
	uint32_t maxSketchSpace = (ESP.getFreeSketchSpace() - 0x1000) & 0xFFFFF000;
	if (!Update.begin(maxSketchSpace, U_FLASH))  //start with max available size
	{
		Update.printError(Serial);
		Serial.println("ERROR");
		return;
	}
	while (file.available()) 
	{
		uint8_t ibuffer[128];
		file.read((uint8_t *)ibuffer, 128);
		Serial.println((char *)ibuffer);
		Update.write(ibuffer, sizeof(ibuffer));
 	}

	Serial.print(Update.end(true));
	digitalWrite(BUILTIN_LED, HIGH);
	file.close();
	server.send(200,"Complate");
  
	ESP.restart();      // 펌웨어 업데이트 하고 재시작
}


void damda_Eeprom_format()
{
	for (int i = 0; i <= EEPROM_SIZE; ++i) 
  	{
		  EEPROM.write(i, 0); 
  	}
  	EEPROM.commit();
  	resetFunc();
}

/* Read eeprom for server address, port, password and wifi information */
void damda_Read_EEPROM()
{	
	Serial.println("Reading EEPROM server_addr, server_port, wifi_ssid, wifi_password and auth key");

	server_addr = "";
	for (int i = 0; i < 32; ++i)				// Read Server Address 
	{
		server_addr += char(EEPROM.read(i));
	}
	Serial.print("server_addr: ");
	Serial.println(server_addr); 
		
	server_port = "";
	for (int i = 32; i < 40; ++i)            	// Read Server Port
	{
		server_port += char(EEPROM.read(i));
	}
	Serial.print("server_port: ");
	Serial.println(server_port);  
		
	wifi_ssid = "";
	for (int i = 40; i < 72; ++i)            	// Read Server SSID
	{
		wifi_ssid += char(EEPROM.read(i));
	}
	Serial.print("wifi_ssid: ");
	Serial.println(wifi_ssid);
		
	wifi_password = "";
	for (int i = 72; i < 104; ++i)            	// Read Server Password
	{
		wifi_password += char(EEPROM.read(i));
	}
	Serial.print("wifi_password: ");
	Serial.println(wifi_password); 
		
	auth_key = "";		
	for (int i = 104; i < 140; ++i)           	// Read Auth key
	{
		auth_key += char(EEPROM.read(i));
	}
	Serial.print("auth_key: ");
	Serial.println(auth_key); 
		
	WiFi_IP_ADDRESS = "";		
	for (int i = 140; i < 172; ++i)    			// Read WiFi IP Address
	{
		WiFi_IP_ADDRESS += char(EEPROM.read(i));
	}
	Serial.print("WiFi_IP_ADDRESS: ");
	Serial.println(WiFi_IP_ADDRESS); 
	  
	damda_SetConnectInfo((char *)server_addr.c_str(),(char *)server_port.c_str(),(char *)wifi_ssid.c_str(),(char *)wifi_password.c_str(),(char *)auth_key.c_str());
	damda_sample_connection_info.serverAddr = server_addr.c_str();
	damda_sample_connection_info.serverPort = server_port.c_str();
	damda_sample_connection_info.apSsid = wifi_ssid.c_str();
	damda_sample_connection_info.apPassword = wifi_password.c_str();
	damda_sample_connection_info.authKey = auth_key.c_str();
	  
	#ifdef _DEBUG_
	Serial.println("\n"); 
	Serial.print("serverAddr : ");
	Serial.println((const __FlashStringHelper *)damda_sample_connection_info.serverAddr);
	Serial.print("serverPort : ");
	Serial.println((const __FlashStringHelper *)damda_sample_connection_info.serverPort);
	Serial.print("apSsid : ");
 	Serial.println((const __FlashStringHelper *)damda_sample_connection_info.apSsid);
	Serial.print("apPassword : ");
	Serial.println((const __FlashStringHelper *)damda_sample_connection_info.apPassword);
	Serial.print("authKey : ");
	Serial.println((const __FlashStringHelper *)damda_sample_connection_info.authKey);
	Serial.print("Device ID: ");
	Serial.println(Topic_Device_id);
	Serial.print("WiFi_IP_ADDRESS : ");
	Serial.println(WiFi_IP_ADDRESS);
 	#endif
    	
	char ipChar[WiFi_IP_ADDRESS.length()+1];
	WiFi_IP_ADDRESS.toCharArray(ipChar,WiFi_IP_ADDRESS.length()+1);
	char *p = ipChar;
	char *str;
	int index=0;
	while ((str = strtok_r(p, ".", &p)) != NULL)
	{
		ip_static[index] = byte(atoi(str));
		index++;
	}
}


/* Interrupt handler, initialize and deregister device */
ICACHE_RAM_ATTR void damda_Reset_EEPROM(void)
{
	//Do not use delay() function in the interrupt handler
	int btn_Status = digitalRead (BUTTON_PIN1); //(BUTTON_PIN);
	if(btn_Status == LOW)
	{
		timer1_disable();
		Serial.println("Start initializing device"); 
		if(damda_IsMQTTConnected())
		{
			Serial.println("Deregister Device");
			damda_Req_DeregisterDevice();
		}
		if (WiFi.status() == WL_CONNECTED) 
		{
			Serial.println("Wifi disconnecting");
			WiFi.disconnect(true); 
		}
		digitalWrite(LED_BUILTIN, HIGH);

		//LittleFS_format();

		Serial.println("EEPROM RESET"); 
		for (int i = 0; i <= EEPROM_SIZE; ++i) 
		{ 
			EEPROM.write(i, 0); 
		}
		EEPROM.commit();
		Serial.println("------ Done device initializing, Restart device ------"); 
		ESP.restart();  //resetFunc() -> uses WDT reset;
	}
}


/* Check the wifi state and if wifi mode is Soft AP than launch web server */
bool damda_Wifi_check(void) 
{
 	int c = 0;
	
	if(booting_wifi_mode  != 2 && booting_wifi_mode != 3)						// only run on Soft AP mode.
	{ 
		launchWeb(1);															// launch WEB server for device pairing.
	}
	Serial.println("at damda_Wifi_check -> Waiting for Wifi to connect");  
	
	while (1) 
	{
		if(booting_wifi_mode  != 2 && booting_wifi_mode != 3)					// only run on Soft AP mode.
		{
			server.handleClient();
		}
		Test_Sample();
	
		if (WiFi.status() == WL_CONNECTED) 
		{
			Serial.print("at damda_Wifi_check -> Local IP: ");
			Serial.println(WiFi.localIP());
			return true; 
		} 

		Serial.print(WiFi.status()); 
		Serial.print("-");      
		// 0 : WL_IDLE_STATUS when Wi-Fi is in process of changing between statuses
		// 1 : WL_NO_SSID_AVAILin case configured SSID cannot be reached
		// 3 : WL_CONNECTED after successful connection is established
		// 4 : WL_CONNECT_FAILED if connection failed
		// 6 : WL_CONNECT_WRONG_PASSWORD if password is incorrect
		// 7 : WL_DISCONNECTED if module is not configured in station mode		
		delay(1000);
	}
	Serial.println("Wifi to Disconnected!");  
	return false;
}


/* Set the Wifi mode by booting_wifi_mode variable, if the booting_wifi_mode is 3 then connect to Mqtt. */
/* if (EEPROM.read(EEPROM_SIZE) == 1) or any --> booting_wifi_mode == 1, Wifi Soft AP mode */
/* if (EEPROM.read(EEPROM_SIZE) == 2)        --> booting_wifi_mode == 2, Wifi Station mode  */
/* if (EEPROM.read(EEPROM_SIZE) == 3)        --> booting_wifi_mode == 3, Wifi Station mode and Mqtt connect  */
void damda_WiFi_Connect()
{
	damda_sample_connection_info = getConnectionInfo();
	
	if(booting_wifi_mode == 2)   		// Wifi station mode 
	{
		Serial.printf("\r\n----------------- WIFI STATION MODE START ----------------- \r\n");
		//Start WIFI Station mode
		WiFi.mode(WIFI_STA);
		WiFi.begin(damda_sample_connection_info.apSsid,damda_sample_connection_info.apPassword);					// Wifi STATION MODE 시작
	}
	else if(booting_wifi_mode == 3)		// Wifi station mode and Mqtt connect  
	{
		Serial.printf("\r\n----------------- WIFI STATION MODE & START MQTT CONNECT ----------------- \r\n");
		
		//damda_WiFi_Connect, debugging point
		//Serial.println("Wifi ssid, Wifi pass, Server Address, Server Port, Device ID : ");
		//Serial.println(damda_sample_connection_info.apSsid);
		//Serial.println(damda_sample_connection_info.apPassword);
		//Serial.println(damda_sample_connection_info.serverAddr);
		//Serial.println(atoi((char*)damda_sample_connection_info.serverPort));
		//Serial.println((char*)Topic_Device_id.c_str());
		//Serial.println(" ");
				
		//Start WIFI Station mode
		WiFi.mode(WIFI_STA);
		WiFi.begin(damda_sample_connection_info.apSsid,damda_sample_connection_info.apPassword);
		delay(500);
		Serial.print("at damda_WiFi_Connect -> Connecting wifi...");
  		while (WiFi.status() != WL_CONNECTED) 
		{
			Serial.print(".");
			delay(500);
		}
		Serial.printf("\r\nat damda_WiFi_Connect -> Wifi connected \r\n");
		// Start to connect Mqtt broker with cert_pem code and get time info from NTP server.		
		damda_Secure();
		// Start connect Mqtt
		damda_MQTT_Connect((char*)Topic_Device_id.c_str(),(char*)damda_sample_connection_info.authKey,(char*)damda_sample_connection_info.serverAddr,atoi((char*)damda_sample_connection_info.serverPort),DEVICE_TYPE);
		
		//damda_WiFi_Connect, debugging point
		//if(damda_IsMQTTConnected()) Serial.println("at damda_WiFi_Connect -> Mqtt connected");

		message_print_flag = 1;
		delay(500);
		EEPROM.write(EEPROM_SIZE, 3);		//Write booting_wifi_mode info to eeprom as "wifi station mode and mqtt connect(3)"
		EEPROM.commit();
	}
	else
	{
		//Start WIFI Soft AP mode
		Serial.printf("\r\n----------------- WIFI SOFT AP MODE START ----------------- \r\n");
		WiFi.mode(WIFI_AP);
		delay(500);
		WiFi.softAPConfig(IPAddress(SOFT_IPSET_STATIC),IPAddress(IPSET_GATEWAY),IPAddress(IPSET_SUBNET));
		delay(500);
		WiFi.softAP(ssid,password);							
		delay(500);
		WiFi.begin(damda_sample_connection_info.apSsid,damda_sample_connection_info.apPassword);			
	}
	
	String data = damda_sample_connection_info.apSsid;
	if(data.length() > 1 && booting_wifi_mode != 1)
	{
    	if (damda_Wifi_check()) 
		{    
			delay(500);
			return;
		}
		//Serial.print("at damda_WiFi_Connect - Local IP: ");
		//Serial.println(WiFi.localIP());
		//Serial.print("at damda_WiFi_Connect - SoftAP IP: ");
		//Serial.println(WiFi.softAPIP());
	}
	else
	{
  		int n = WiFi.scanNetworks();
		Serial.println("scanNetworks done");
		if (n == 0)
		{
			Serial.println("no networks found");
		}
		else
		{
			Serial.print(n);
			Serial.println(" networks found"); // Output Wifi APs information. 
			for (int i = 0; i < n; ++i)
			{
				// Print SSID and RSSI for each network found.
				Serial.print(i + 1);
				Serial.print(": ");
				Serial.print(WiFi.SSID(i));
				Serial.print(" (");
				Serial.print(WiFi.RSSI(i));
				Serial.print(")");
				Serial.println((WiFi.encryptionType(i) == ENC_TYPE_NONE)?" ":"*");
				delay(10);
			}
		}
		st = "<ol>";
		for (int i = 0; i < n; ++i)
		{
			// Print SSID and RSSI for each network found
			st += "<li>";
			st += WiFi.SSID(i);
			st += " (";
			st += WiFi.RSSI(i);
			st += ")";
			st += (WiFi.encryptionType(i) == ENC_TYPE_NONE)?" ":"*";
			st += "</li>";
		}
		st += "</ol>";
		delay(100);
		damda_Wifi_check(); // is called on Soft AP mode
  	}
}


/* Set the Wifi reconnection information */
void damda_WiFi_Reconnect()
{
  	server.send(200, "application/json", "{}"); 
	delay(1000);
  	
	StaticJsonDocument<300> newBuffer;
	deserializeJson(newBuffer, server.arg("plain"));                              //  buffer에 body 내용 json으로 변환
	JsonObject bodyContent = newBuffer.as<JsonObject>();                          //  json 내용 jsonObject에 할당
  	
	JsonVariant wifi_ssidVariant = bodyContent.getMember("ap_ssid");
	wifi_ssid = wifi_ssidVariant.as<const char*>();
	JsonVariant wifi_passwordVariant = bodyContent.getMember("ap_password");
	wifi_password = wifi_passwordVariant.as<const char*>();
  	
	damda_sample_connection_info.apSsid = wifi_ssid.c_str();
	damda_sample_connection_info.apPassword = wifi_password.c_str();
	  	
	Serial.print("WiFi_ssid : ");						// Wifi ID
	Serial.println(damda_sample_connection_info.apSsid);
	Serial.print("WiFi_password : ");					// Wifi Password
	Serial.println(damda_sample_connection_info.apPassword);
  	
	EEPROM.commit();
	Serial.println("EEPROM Erassing Complate");
	delay(100);
	
	if (wifi_ssid.length() > 0)
	{
		for (int i = 0; i < wifi_ssid.length(); ++i)
		{
			EEPROM.write(40+i, wifi_ssid[i]);
			Serial.print("wifi_ssid: ");
			Serial.println(wifi_ssid[i]); 
		}
		for (int i = 0; i < wifi_password.length(); ++i)
		{
			EEPROM.write(72+i, wifi_password[i]);
			Serial.print("wifi_password: ");
			Serial.println(wifi_password[i]); 
		}
	}
	EEPROM.commit();
	delay(100);
	ESP.restart();
}


/* Timer interrupt service routine, increase the timer variables whenever interrupt occured. */
/* The timer variables are used on 'loop' function to receive the message and send the message from/to server. */
/* Do not put Serial print function in the code. */
void Timer1_interrup_10m() 
{
	Timer_10ms_ReceiveCallBack++;
	Timer_10ms_SendMessage++;
}


/* Device GPIO setting. */
void Device_Port_init()
{
  	pinMode(BUTTON_PIN, INPUT_PULLUP);    	//버튼
	//use BUTTON_PIN1(gpio 5) as interrupt 
	pinMode(BUTTON_PIN1, INPUT_PULLUP);    	//버튼
	attachInterrupt(digitalPinToInterrupt(BUTTON_PIN1), damda_Reset_EEPROM, FALLING);
	
	pinMode(LED_BUILTIN, OUTPUT);       	//상태 LED  기본장착
	#ifdef DEVICE_SWITCH
	pinMode(BUTTON_LED1, OUTPUT);      	  	//POWER LED
	pinMode(BUTTON_LED2, OUTPUT);    	    //LED1
	pinMode(BUTTON_LED3, OUTPUT);       	//LED2
	pinMode(BUTTON_LED4, OUTPUT);    	    //LED3
	pinMode(DIMMING_LED, OUTPUT);    	    //DEMING

	digitalWrite(BUTTON_LED1, LOW);
	digitalWrite(BUTTON_LED2, LOW);
	digitalWrite(BUTTON_LED3, LOW);
	digitalWrite(BUTTON_LED4, LOW);
	digitalWrite(DIMMING_LED, LOW);
	#endif

	#ifdef DEVICE_PLUG
	pinMode(BUTTON_LED1, OUTPUT);      	  	//POWER LED
	pinMode(BUTTON_LED2, OUTPUT);    	    //LED1
	pinMode(BUTTON_LED3, OUTPUT);       	//LED2
	pinMode(BUTTON_LED4, OUTPUT);    	    //LED3

	digitalWrite(BUTTON_LED1, LOW);
	digitalWrite(BUTTON_LED2, LOW);
	digitalWrite(BUTTON_LED3, LOW);
	digitalWrite(BUTTON_LED4, LOW);
	#endif
	digitalWrite(LED_BUILTIN, LOW);
}


void _printf(const char *s,...)
{
	va_list args;
	va_start(args, s);
	int n = vsnprintf(NULL, 0, s, args);
	char *str = new char[n+1];
	vsprintf(str, s, args);
	va_end(args);
	Serial.print(str);
	delete [] str;
}


/* Setup function that execute once when devie power on. */
/* This function initialize the IOs, Wifi, Timer interrupt and try to connect to Mqtt. */
void setup() 
{
	// put your setup code here, to run once:
	DeviceStatusInfo damda_deviceStatusInfo;

	Serial.begin(115200);
	#ifdef DEVICE_PLUG
	T_Serial.begin(9600);           				// Open RS232 serial port to communicate with device's control board. 
	#endif

	//Serial.printf("at setup #1 : Free Heap size = %d \r\n", ESP.getFreeHeap());	

  	EEPROM.begin(255);                				// EEPROM Start 
	booting_wifi_mode = EEPROM.read(EEPROM_SIZE); 	// Read boot mode information from eeprom at last address, EEPROM_SIZE == last address.
	Device_Port_init();
	WiFi.disconnect(true); 
	damda_setupDeviceInfo(DEVICE_MANUFACTURER, DEVICE_MODEL, DEVICE_SERIAL, "1.0", DEVICE_TYPE);
 	damda_Read_EEPROM();

	damda_WiFi_Connect();							// Set Wifi mode and try to connect to Mqtt.
	delay(1000);

	printLocalTime();								// Copy time info to ti(unix timestamp)
	damda_setMQTTMsgReceiveCallback(message_callback);
	damda_deviceStatusInfo = getDeviceInfo();
	Serial.print("device_model :");
	Serial.println(damda_deviceStatusInfo.serial);
	damda_getSDKVersion();
	damda_LittleFS();
	message_print_flag = 1;							// Outpu menu list through serial port

	delay(1000);
	wdt_enable(WDTO_1S);      					    // Enable software Watch dog with 1 second

	if(!damda_IsMQTTConnected())
	{
		Serial.println(" ");
		Serial.println("****************************************************");
		Serial.println("at setup : Mqtt is not connected, device will reboot");
		Serial.println("****************************************************");
		resetFunc();
	}
	damda_Req_RegisterDevice();
	delay(500);
	Notify_sync();

	timer1_isr_init();                  				// Initlize timer interrupt  //Mqtt 연결전에 타이머 동작시 오류 납니다.
	timer1_attachInterrupt(Timer1_interrup_10m);        // Register timer interrupt routine
	timer1_enable(1,0,1);								// Enable timer interrupt
	timer1_write(50000);                       		   	// Set timer interval :: 5,000,000 = 1 second, 50,000 = 10ms, 5,000 = 1ms

	//Serial.printf("at setup #2: Free Heap size = %d \r\n", ESP.getFreeHeap());
}


/* Loop function that execute forever. */
/* This function handles received message from the platform and send the message to the platform periodically. */
void loop() 
{
	if(Timer_10ms_ReceiveCallBack >= 10)  				// every 0.1s, handle reviced message from IoT platform. 
  	{
    	Timer_10ms_ReceiveCallBack = 0;
		damda_messageReceived_callback_loop();    		// Handle recived message (MQTT Call back monitoring)
		Test_Command();                     			// 테스트 컴멘더 리스트 출력
		Test_Sample();                      			// 테스트 컴멘더 입력 및 실행
		//control();  	                   
		LED_Contol();                    				// LED 제어
	}

	if(Timer_10ms_SendMessage >= sendmessage_period_minute)			// every 1 minute (100*60*1), send message to IoT platform
	{
		Timer_10ms_SendMessage = 0;

     	if (WiFi.status() == WL_CONNECTED)							// If Wifi is normaly connected, periodically send message data to the DAMDA platform
		{
			if(ucNotify_sync_flag == 1 || ucNotify_async_flag == 1)
			{
				if(!damda_IsMQTTConnected())
				{
					Serial.println("at loop : Done damda_secure");
					damda_MQTT_Connect((char*)Topic_Device_id.c_str(),(char*)damda_sample_connection_info.authKey,(char*)damda_sample_connection_info.serverAddr,atoi((char*)damda_sample_connection_info.serverPort),DEVICE_TYPE);
					delay(100);
					Serial.println("at loop : Mqtt connected");
				}
				else
				{
					Serial.println("at loop : called Notify_sync ~~~");
					Notify_sync();
					//Serial.printf("at loop : Free Heap size = %d \r\n", ESP.getFreeHeap());
				}
			}
		}
		else														// If Wifi is not connected, device will reboot.
		{
			Serial.println(" ");
			Serial.println("****************************************************");
			Serial.println("at loop : Wifi is disconnected, device will reboot");
			Serial.println("****************************************************");
			resetFunc();
		}
  	}
}
